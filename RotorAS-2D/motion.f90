    !*******************************************************************
    !	Copyright(c), 2016-2026, Sun Zhenhang
    !   All rights reserved.
    !	
    !	File name: motion
    !	Version: 2.0
    !	Author: Sun Zhenhang
    !	Date: 2016-4-28
    !
    !   Description: module used to deal wich moving grid problem
    !
    !*******************************************************************

    
module motion
    
    use controlDict,only: Ma_inf, rou_inf, p_inf, gamma, pi
    use controlDict,only: kr, att, att_ampl, rot_cen
    use gridInfo,only: nnodes, nedges, ncells,xy, vector, rij, rR, rL, tij    
    implicit none
    
    real(8),save::angularFrequency
    real(8),allocatable,save::xy0(:,:),vector0(:,:),rij0(:,:),rR0(:,:),rL0(:,:),tij0(:,:)
    
    !real(8),allocatable::U_Rot(:,:)
    !------------------------------------------------------
                ! variables specification
    ! angularFrequency           :      angular frequency computed from kr
    ! xy0,vector0,...            :      stores the initial grid information before rotating.         
    ! U_Rot                      :      rotating speed of each node.
    !-----------------------------------------------------
contains
    
subroutine allocateMemory_moving
    !*******************************************************************
    !   Description: 
    !	Date: 2016-4-28
    !   Modification: 
    !*******************************************************************
    
    implicit none
    
    allocate( xy0(2,nnodes) )
    allocate( vector0(2,nedges) )
    allocate( rij0(2,nedges) ) 
    allocate( rR0(2,nedges) ) 
    allocate( rL0(2,nedges) )
    allocate( tij0(2,nedges) )
    
    !allocate( U_Rot(2,nnodes) )
    
end subroutine

subroutine deallocateMemory_moving
    !*******************************************************************
    !   Description: 
    !	Date: 2016-4-28
    !   Modification: 
    !*******************************************************************

    implicit none
    
    deallocate( xy0 )
    deallocate( vector0 )
    deallocate( rij0 ) 
    deallocate( rR0 ) 
    deallocate( rL0 )
    deallocate( tij0)
    
    !allocate( U_Rot(2,nnodes) )
    
end subroutine

subroutine  motionInit
    !*******************************************************************
    !   Description: 
    !	Date: 2016-4-28
    !   Modification: 
    !*******************************************************************
    
    implicit none
    integer::i
    
    call allocateMemory_moving
    
    vector0 = vector
    rij0 =rij
    rR0 = rR
    rL0 = rL
    tij0 = tij
    
    ! set rotational center's coordinates to zero.
    do i=1,nnodes
        xy0(:,i) = xy(:,i)-rot_cen(:)    
    end do 
    
    angularFrequency =  2.0d0*kr*Ma_inf*sqrt(gamma*p_inf/rou_inf) ! omg = 2.0*kr*Ma*a
    
         
end subroutine

subroutine rotation( t,U_Rot,AOA ) 
    !*******************************************************************
    !   Description: get new geometric information after rotation
    !	Date: 2016-4-28
    !   Modification: 
    !*******************************************************************
       
    implicit none 
    real(8),intent(in):: t
    real(8),intent(out)::U_Rot(:,:)
    real(8),intent(out)::AoA                     ! attact angle              
    real(8)::angular_rate            ! angular velocity 
    
    AoA = att + att_ampl* sin( angularFrequency * t )
    angular_rate = angularFrequency * ( att_ampl/180.0d0 *pi ) *cos( angularFrequency * t)
    
    ! all the geometric information has to be renewed with grid rotating
     
    call spin(xy,xy0,AOA)
    !xy(1,:) =   xy0(1,:) * cosd(AoA-att) + xy0(2,:) * sind(AoA-att) 
    !xy(2,:) = - xy0(1,:) * sind(AoA-att) + xy0(2,:) * cosd(AoA-att) 
         
    U_Rot(1,:) =  angular_rate *  xy(2,:)   
    U_Rot(2,:) = -angular_rate *  xy(1,:)      
              
    call spin(vector,vector0,AOA)
    !vector(1,:) =  vector0(1,:) * cosd( AoA-att ) + vector0(2,:) * sind( AoA-att )
    !vector(2,:) = -vector0(1,:) * sind( AoA-att ) + vector0(2,:) * cosd( AoA-att )   
    
    call spin(rij,rij0,AOA)
    !rij(1,:) =  rij0(1,:) * cosd( AoA-att ) + rij0(2,:) * sind( AoA-att )
    !rij(2,:) = -rij0(1,:) * sind( AoA-att ) + rij0(2,:) * cosd( AoA-att )
    
    call spin(rR,rR0,AOA)
    !rR(1,:) =  rR0(1,:) * cosd( AoA-att ) + rR0(2,:) * sind( AoA-att )
    !rR(2,:) = -rR0(1,:) * sind( AoA-att ) + rR0(2,:) * cosd( AoA-att )
    
    call spin(rL,rL0,AOA)
    !rL(1,:) =  rL0(1,:) * cosd( AoA-att ) + rL0(2,:) * sind( AoA-att )
    !rL(2,:) = -rL0(1,:) * sind( AoA-att ) + rL0(2,:) * cosd( AoA-att )
    
    call spin(tij,tij0,AOA)
    !tij(1,:) =  tij0(1,:) * cosd( AoA-att ) + tij0(2,:) * sind( AoA-att )
    !tij(2,:) = -tij0(1,:) * sind( AoA-att ) + tij0(2,:) * cosd( AoA-att )
    
                
end subroutine

subroutine spin( newVar,oldVar,AOA)
    !*******************************************************************
    !   Description:  rotote around origin 
    !	Date: 2016-4-28
    !   Modification: 
    !*******************************************************************
 
    real(8),intent(out)::newVar(:,:)
    real(8),intent(in)::oldVar(:,:)
    
    real(8),intent(in)::AOA
    
    newVar(1,:) =  oldVar(1,:) * cosd( AoA-att ) + oldVar(2,:) * sind( AoA-att )
    newVar(2,:) = -oldVar(1,:) * sind( AoA-att ) + oldVar(2,:) * cosd( AoA-att )
end subroutine


end module
    