    !*******************************************************************
    !	Copyright(c), 2016-2026, Sun Zhenhang
    !   All rights reserved.
    !	
    !	File name: gridInfo
    !	Version: 2.0
    !	Author: Sun Zhenhang
    !	Date: 2016-4-28
    !
    !   Description: attain geometric information
    !
    !*******************************************************************
           
module gridInfo
  
    use controlDict,only: airfoilName, dirGrid
    
    implicit none
    integer,save::nnodes,ncells,nedges 
    integer,allocatable,save:: iedge(:,:),icell(:,:)
    real(8),allocatable,save:: xy(:,:),vol(:)
    real(8),allocatable,save:: vector(:,:),ds(:)             
    real(8),allocatable,save:: cell_center(:,:)
    real(8),allocatable,save:: rL(:,:),rR(:,:)
    real(8),allocatable,save:: rij(:,:),lij(:),tij(:,:)
    
    real(8),allocatable,save::d(:)
    
    !-------------------------------------------------------
    !              Variables Specification
    !nnodes     :   node numbers
	!ncells     :   cell numbers
	!nedges     :   edge numbers
	!iedge      :   matrix for edge
		            !iedge(1,i) : edge'S start point 'a'
		            !iedge(2,i) : edge'S end point 'b'
		            !iedge(3,i) : edge'S left cell  (all positive)
		            !iedge(4,i) : positive for right cell
		                        !iedge(4,i) -1 for wall boundary
		                        !iedge(4,i) -2 for farfiled boundary
	!xy         :   cartesian coordinates  xy(1,i) for x  xy(2,i) for y
	!icell      :   triangle cell'S three nodes index
	!vol        :   cell'S volume(area in 2d)
    !--------------------------------------------------------
    
contains
    
subroutine deallocateMemory_Grid
    !*******************************************************************
    !   Description: 
    !	Date: 2016-4-28
    !   Modification: 
    !*******************************************************************
    
    deallocate(iedge)
    deallocate(xy)
    deallocate(icell)
    deallocate(vol)
    deallocate(vector)               !the normal vector  of every edge
    deallocate(ds)                   !the length of every edge
    deallocate(cell_center)
    deallocate(rL)
    deallocate(rR)
    deallocate(rij)
    deallocate(lij)
    deallocate(tij)
    deallocate( d)
    
end subroutine   

subroutine grid

    !*******************************************************************
    !   Description: 
    !	Date: 2016-4-28
    !   Modification: 
    !*******************************************************************
    
    call readGrid     ! read topological data of the grid
    
    call gridData     ! get gemometric information 
    call Distance     ! get the minimum distance of the center of each cell to the wall    
end subroutine


subroutine  readGrid

    !*******************************************************************
    !   Description: read topological data from grid file
    !	Date: 2016-4-28
    !   Modification: 
    !*******************************************************************
    
    implicit none
	integer::i	
    integer::gridid
       
    open(gridid,file=trim(adjustL(dirGrid))//"grid/"//trim(adjustL(airfoilName))//".grd")
      
        read(gridid,*)  nnodes,nedges,ncells
            
        !allocate memory
        allocate(iedge(4,nedges))
        allocate(xy(2,nnodes))
        allocate(icell(4,ncells))
        allocate(vol(ncells))
            
        do i=1,nnodes
            read(gridid,*)   xy(:,i)
        end do
            
        do i=1,nedges
            read(gridid,*)  iedge(:,i)
        end do
        
        do i=1,ncells
            read(gridid,*)  icell(:,i)
        end do
            
        do i=1,ncells
            read(gridid,*)  vol(i)
        end do
    close(gridid)

end subroutine
 
subroutine  gridData
    !*******************************************************************
    !   Description: get geometric information from topological data 
    !	Date: 2016-4-28
    !   Modification: 
    !*******************************************************************
    implicit none
    integer::i
    integer::start_node,end_node,ncl,ncr

    !allocate memory
    allocate(vector(2,nedges))               ! the normal vector  of every edge
    allocate(ds(nedges))                     ! the  length of every edge
    allocate(cell_center(2,ncells))
    allocate(rL(2,nedges))
    allocate(rR(2,nedges))
    allocate(rij(2,nedges))
    allocate(lij(nedges))
    allocate(tij(2,nedges))

    !the normal vector and the length of every edge, clockwise
    vector(1,:)= xy(2,iedge(2,:)) - xy(2,iedge(1,:))         !  dy
    vector(2,:)=-xy(1,iedge(2,:)) + xy(1,iedge(1,:))         !  -dx
    ds(:)=sqrt( vector(1,:)**2+vector(2,:)**2 )
    !-------------------------------------------------------
    do i=1,ncells
        if( icell(3,i) /= icell(4,i) )  then
            cell_center(:,i) =  sum( xy(:,icell(:,i) ),2)/4.0d0
            !cell_center(:,i) = ( xy(:,icell(1,i) ) + xy(:,icell(2,i) ) + xy(:,icell(3,i) ) + xy(:,icell(4,i) ) ) /4.0d0
        else
            cell_center(:,i) =  sum( xy(:,icell(1:3,i) ),2)/3.0d0
            !cell_center(:,i) = ( xy(:,icell(1,i) ) + xy(:,icell(2,i) ) + xy(:,icell(3,i) )  ) /3.0d0
        end if
    end do
    !-------------------------------------------------------

    do i=1,nedges
        start_node = iedge(1,i)
        end_node = iedge(2,i)
        ncl = iedge(3,i)
        ncr = iedge(4,i)
        select case(ncr)
        case(-2:-1)
            ! rL: the left cell center --> edge center, it's used to calculate the gradient of every edge
            ! rij: the left cell center --> the right cell center
            rL(:,i) = 0.5d0*( xy(:,start_node) + xy(:,end_node) )  - cell_center(:,ncl)
            rij(:,i) = 2.0d0 * rL(:,i)
            case default
            rL(:,i) = 0.5d0*( xy(:,start_node) + xy(:,end_node) )  - cell_center(:,ncl)
            rR(:,i) = 0.5d0*( xy(:,start_node) + xy(:,end_node) )  - cell_center(:,ncr)
            rij(:,i) =  cell_center(:,ncr)-cell_center(:,ncl)   
        end select

    end do

    lij= sqrt(rij(1,:)**2 + rij(2,:)**2 )
    tij(1,:) = rij(1,:) /lij
    tij(2,:) = rij(2,:) /lij

end subroutine

subroutine Distance 
    !*******************************************************************
    !   Description: get the minimux distance from cell center to wall
    !	Date: 2016-4-28
    !   Modification: 
    !*******************************************************************
    
    implicit none
    integer::i,j,k
    integer::ncl,ncr
    integer::num 

    real(8)::d_temp
    real(8),allocatable::coor_b(:,:)

    allocate( d(ncells) )

    ! get the number of boundary edge 
    num = 0
    do i=1,nedges
        ncr = iedge(4,i)
        if(ncr == -1)  num = num + 1  
    end do

    allocate( coor_b(2,num) )

    j=0
    do i=1,nedges
        ncr = iedge(4,i)
        if(ncr == -1)  then
            j = j + 1
            coor_b(:,j) =  0.5d0*( xy(:,iedge(1,i) )  + xy(:,iedge(2,i) ) ) 
        end if
    end do

    do i =1,ncells
        d(i) = sqrt( sum( (cell_center(:,i) - coor_b(:,1) )**2 ))
        do j=2,num
            d_temp = sqrt( sum( (cell_center(:,i) - coor_b(:,j) )**2))
            if ( d_temp < d(i) )  d(i) = d_temp
        end do   
        if(i==1)  write(*,*) ! necessary, otherwise error will occur. Don't konw why.  
    end do

    deallocate( coor_b)

end subroutine

end module