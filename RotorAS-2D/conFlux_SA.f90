subroutine conFlux_SA(U_av,U_Rot)     
    !*******************************************************************
    !   Description:  convective flux using central scheme
    !	Date: 2016-4-28
    !   Modification: 
    !*******************************************************************
    implicit none
    real(8),intent(in)::U_av(:,:),U_Rot(:,:)     
    
    integer::i
    integer::ncl,ncr                                       !the number of the left and the right cell
                           
    real(8)::ux,uy,Vn                                     
                              
    !set Fc_SA to zero
   
    Fc_SA = 0.0d0
    
    do i = 1,nedges
        ncl=iedge(3,i)
        ncr=iedge(4,i)
        
        ux =  U_av(2,i) - 0.5d0 * ( U_Rot(1,iedge(1,i)) + U_Rot(1,iedge(2,i)) )
        uy =  U_av(3,i) - 0.5d0 * ( U_Rot(2,iedge(1,i)) + U_Rot(2,iedge(2,i)) )
        Vn = ux* vector(1,i)   + uy * vector(2,i) 
     
        Fc_SA(ncl)  = Fc_SA(ncl) + U_av(1,i)*U_av_SA(i) * Vn
        
        if ( ncr > 0) then
            !the left cell plus the convective flux
            Fc_SA(ncr)  = Fc_SA(ncr) - U_av(1,i)*U_av_SA(i) * Vn
        end if

    end do
    
end subroutine
