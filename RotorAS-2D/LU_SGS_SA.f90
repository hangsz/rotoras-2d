subroutine LU_SGS_SA    
    !*******************************************************************
    !   Description: Lower-Upper Symmetric Successive Overrelexation scheme, get W(n+1)  at every time step
    !	Date: 2016-4-28
    !   Modification: 
    !******************************************************************* 
    implicit none

    integer::i,j,k
    
    integer(8)::edge,cell_num,cell_lower,cell_upper
    
    real(8)::sign
    real(8)::ut,vt,Vnt
    real(8)::ux,uy,Vn,c,rA_v,rA
    real(8)::rA_T
    real(8)::U_temp(5),W_temp(5),Fc_n1(5),Fc_n(5),FcTemp(5)
    real(8)::dFc(5)
    real(8)::diag(5)
    real(8),allocatable::dw1(:,:)
    real(8),allocatable::dwn(:,:)
    
    !turbulence
    real(8)::WT_temp,Fc_WT_n1,Fc_WT_n
    real(8)::dFc_WT
    real(8)::diag_WT
    real(8),allocatable::dWT1(:)
    real(8),allocatable::dWTn(:)
    
    integer::error

    !-----------------------------------------
    !write(*,*)  "LU-SGS"
    allocate( dw1(5,ncells),stat = error)
    if( error /= 0) write(*,*) "dw1 allocate Error"
    
    allocate( dwn(5,ncells),stat = error ) 
    if( error /= 0) write(*,*) "dwn allocate Error"
    
    allocate( dWT1(ncells),stat = error)
    if( error /= 0) write(*,*) "dWT1 allocate Error"
    allocate( dWTn(ncells),stat = error)
    if( error /= 0) write(*,*) "dWTn allocate Error"
    
    
    !------------------------------------------------------------------- 
    dw1 = 0.0d0
    dwn = 0.0d0
    
    dWT1 = 0.0d0
    dWTn = 0.0d0
       
    !Forward  sweep
    do i=1,ncells   !it doesn't mean that the sequence is identical to the cell order
        
        cell_num = assigned(i)  
          
        dFc = 0.0d0  
        dFc_WT = 0.0d0
         
        do j=1,4 
            edge = lower( j,cell_num )   !each cell has three lower edges totally(in fact,it can only have two lower or upper edges at most)   
            if( edge == 0)  exit
           
            !determine the cell number lower than the current cell
            !because only the lower edge is stored , it has to be tested which cell is the lower cell

            if( iedge(3,edge) /= cell_num ) then
                cell_lower = iedge(3,edge)
                sign = -1.0d0
            else
                cell_lower = iedge(4,edge)
                sign = 1.0d0
            end if
            !-------------------------------------
            !convective flux, calculate  the lower cells'S_SA n+1 step convective flux 
            W_temp = W(:,cell_lower) + dw1(:,cell_lower)  
            
            WT_temp = W_SA(cell_lower) + dWT1(cell_lower)  
            
            U_temp(1) = W_temp(1)
            U_temp(2) = W_temp(2)/W_temp(1)
            U_temp(3) = W_temp(3)/W_temp(1)
            U_temp(4) = 0.0d0
            U_temp(5) = (gamma-1.0d0)*( W_temp(5)-U_temp(1)*( U_temp(2)**2 + U_temp(3)**2)/2.0d0 )  
            
            if( icell(3,cell_lower)/=icell(4,cell_lower) ) then
                ut = 0.25d0 * sum( U_Rot(1,icell(:,cell_lower)) )
                vt = 0.25d0 * sum( U_Rot(2,icell(:,cell_lower)) )
            else
                ut = 1.0d0/3.0d0 * sum( U_Rot(1,icell(1:3,cell_lower)) )
                vt = 1.0d0/3.0d0 * sum( U_Rot(2,icell(1:3,cell_lower)) )
            end if
            
            Vnt = ut*vector(1,edge) + vt*vector(2,edge)
            
            ux =  U_temp(2) - ut
            uy =  U_temp(3) - vt
            Vn = ux * vector(1,edge)   + uy * vector(2,edge) 
            
            Fc_n1(1) = W_temp(1) * Vn
            Fc_n1(2) = W_temp(2) * Vn + vector(1,edge) * U_temp(5)
            Fc_n1(3) = W_temp(3) * Vn + vector(2,edge) * U_temp(5)
            Fc_n1(4) = 0.0d0
            Fc_n1(5) = ( W_temp(5) + U_temp(5) ) * Vn
            Fc_n1(5) = Fc_n1(5) + Vnt*U_temp(5)
            
            Fc_WT_n1 = WT_temp * Vn
           
            !calculate  the lower cells'S_SA n step convective flux 
            ux =  U(2,cell_lower) - ut
            uy =  U(3,cell_lower) - vt 
            Vn = ux * vector(1,edge)   + uy * vector(2,edge)
       
            Fc_n(1) = W(1,cell_lower) * Vn
            Fc_n(2) = W(2,cell_lower) * Vn + vector(1,edge) * U(5,cell_lower)
            Fc_n(3) = W(3,cell_lower) * Vn + vector(2,edge) * U(5,cell_lower)
            Fc_n(4) = 0.0d0
            Fc_n(5) = ( W(5,cell_lower) + U(5,cell_lower) )* Vn
            Fc_n(5) = Fc_n(5) + Vnt * U(5,cell_lower)
            
            Fc_WT_n = W_SA(cell_lower) *Vn
                
            !-----------------------------------------------
            ! precondition
            ! the convective flux has to modify
            if( isPrecondition==1) then
                FcTemp = Fc_n1
                do k=1,5
                    Fc_n1(k) = dot_product( MInvSP(k,:,cell_lower),FcTemp )
                end do
                FcTemp = Fc_n
                do k=1,5
                    Fc_n(k) = dot_product( MInvSP(k,:,cell_lower),FcTemp )
                end do
            
                rA   = omg*SPalf_c(edge) + SPalf_v(edge)*ds(edge)/lij(edge)
                rA_T = omg*alf_c(edge)   + alf_v(edge)*ds(edge)/lij(edge)
            else
                rA   = omg*alf_c(edge) + alf_v(edge)*ds(edge)/lij(edge)
                rA_T = rA
            end if 
            
            !----------------------------------------------------------------------

            dFc = dFc + sign*(Fc_n1 - Fc_n ) -  rA*dw1(:,cell_lower)  
            dFc_WT = dFc_WT + sign*(Fc_WT_n1 - Fc_WT_n ) -  rA_T*dWT1(cell_lower)     
            
        end do 
    
        if( isPrecondition == 1) then
            diag = vol(cell_num)/dt(cell_num)  + omg/2.0d0*SPlamda_c(cell_num) + SPlamda_v(cell_num)
            diag_WT = vol(cell_num)*( 1.0/dt(cell_num) + 1.5d0/dt_r ) + omg/2.0d0*lamda_c(cell_num) + lamda_v(cell_num) - vol(cell_num)*dQdW_SA(cell_num) 
        else
            diag = vol(cell_num)*( 1.0d0/dt(cell_num) + 1.5d0/dt_r ) + omg/2.0d0*lamda_c(cell_num) + lamda_v(cell_num)
            diag_WT = vol(cell_num)*( 1.0d0/dt(cell_num) + 1.5d0/dt_r ) + omg/2.0d0*lamda_c(cell_num) + lamda_v(cell_num) - vol(cell_num)*dQdW_SA(cell_num) 
        end if

        dw1(:,cell_num) = 1.0d0/diag * ( - Rsi(:,cell_num) - 0.5d0*dFc )
        
        dWT1(cell_num) = 1.0d0/diag_WT * ( - Rsi_SA(cell_num) - 0.5d0*dFc_WT )

    end do    
    
    !Backward sweep
    do i = ncells,1,-1
        
        cell_num = assigned(i)    
        
        dFc = 0.0d0
        dFc_WT=0.0d0
        
        do j=1,4
            
            edge = upper( j,cell_num )
            if( edge == 0 )  exit
                
            if( iedge(3,edge) /= cell_num ) then
                cell_upper = iedge(3,edge)
                sign = -1.0d0
            else
                cell_upper = iedge(4,edge)
                sign = 1.0d0
            end if
                         
            !-------------------------------------------------
            !calculate the upper cells'S_SA n+1 convective flux 
            W_temp = W(:,cell_upper) + dwn(:,cell_upper)  
            WT_temp = W_SA(cell_upper) + dWTn(cell_upper)  
            
            U_temp(1) = W_temp(1)
            U_temp(2) = W_temp(2)/W_temp(1)
            U_temp(3) = W_temp(3)/W_temp(1)
            U_temp(4) = 0.0d0
            U_temp(5) = (gamma-1.0d0)*( W_temp(5)-U_temp(1)*( U_temp(2)**2 + U_temp(3)**2)/2.0d0 )  
            
            if( icell(3,cell_upper)/=icell(4,cell_upper) ) then
                ut = 0.25d0 * sum( U_Rot(1,icell(:,cell_upper)) )
                vt = 0.25d0 * sum( U_Rot(2,icell(:,cell_upper)) )
            else
                ut = 1.0d0/3.0d0 * sum( U_Rot(1,icell(1:3,cell_upper)) )
                vt = 1.0d0/3.0d0 * sum( U_Rot(2,icell(1:3,cell_upper)) )
            end if
            Vnt = ut * vector(1,edge)   + vt * vector(2,edge)
            
            ux =  U_temp(2) - ut
            uy =  U_temp(3) - vt
            Vn = ux * vector(1,edge)   + uy * vector(2,edge) 
            
            Fc_n1(1) = W_temp(1) * Vn
            Fc_n1(2) = W_temp(2) * Vn + vector(1,edge) *U_temp(5)
            Fc_n1(3) = W_temp(3) * Vn + vector(2,edge) *U_temp(5)
            Fc_n1(4) = 0.0d0
            Fc_n1(5) = ( W_temp(5) + U_temp(5) ) * Vn
            Fc_n1(5) = Fc_n1(5) + Vnt*U_temp(5)
            
            Fc_WT_n1=WT_temp * Vn
            
            !calculate  the upper cells's n step convective flux 
            
            ux =  U(2,cell_upper) - ut
            uy =  U(3,cell_upper) - vt 
            Vn = ux * vector(1,edge)   + uy * vector(2,edge)
 
            Fc_n(1) = W(1,cell_upper) * Vn
            Fc_n(2) = W(2,cell_upper) * Vn + vector(1,edge) *U(5,cell_upper)
            Fc_n(3) = W(3,cell_upper) * Vn + vector(2,edge) *U(5,cell_upper)
            Fc_n(4) = 0.0d0
            Fc_n(5) = ( W(5,cell_upper) + U(5,cell_upper) )* Vn
            Fc_n(5) = Fc_n(5) +  Vnt*U(5,cell_upper) 
            
            Fc_WT_n = W_SA(cell_upper) *Vn
            
            !-----------------------------------------------
            ! the convective flux has to modify
            !precondition
            if( isPrecondition==1) then
                FcTemp = Fc_n1
                do k=1,5
                    Fc_n1(k) = dot_product( MInvSP(k,:,cell_upper),FcTemp )
                end do
                FcTemp = Fc_n
                do k=1,5
                    Fc_n(k) = dot_product( MInvSP(k,:,cell_upper),FcTemp )
                end do
            
                rA = omg*SPalf_c(edge) + SPalf_v(edge)*ds(edge)/lij(edge)
                rA_T = omg*alf_c(edge) + alf_v(edge)*ds(edge)/lij(edge)
            else
                rA = omg*alf_c(edge) + alf_v(edge)*ds(edge)/lij(edge)
                rA_T = rA
            end if
            
            !--------------------------------------------------
            dFc = dFc + sign*( Fc_n1 - Fc_n ) - rA*dwn(:,cell_upper)        
            dFc_WT = dFc_WT + sign*( Fc_WT_n1 - Fc_WT_n ) - rA_T*dWTn(cell_upper)          
            
        end do
        
        if( isPrecondition==1) then
            diag    = vol(cell_num)/dt(cell_num)  + omg/2.0d0*SPlamda_c(cell_num) + SPlamda_v(cell_num)
            diag_WT = vol(cell_num)*( 1.0d0/dt(cell_num) + 1.5d0/dt_r ) + omg/2.0d0*lamda_c(cell_num) + lamda_v(cell_num) - vol(cell_num)*dQdW_SA(cell_num) 
        else
            diag    = vol(cell_num)*( 1.0d0/dt(cell_num) + 1.5d0/dt_r ) + omg/2.0d0*lamda_c(cell_num) + lamda_v(cell_num)
            diag_WT = vol(cell_num)*( 1.0d0/dt(cell_num) + 1.5d0/dt_r ) + omg/2.0d0*lamda_c(cell_num) + lamda_v(cell_num) - vol(cell_num)*dQdW_SA(cell_num) 
        end if
        
        dwn(:,cell_num) = dw1(:,cell_num) -  0.5d0*dFc/diag
        dWTn(cell_num) = dWT1(cell_num) -  0.5d0*dFc_WT/diag_WT
        
    end do
          
    W = W + dwn 
    W_SA = W_SA + dWTn 
    !calculate the original variables
    
    U(1,:) = W(1,:)
    U(2,:) = W(2,:)/W(1,:)
    U(3,:) = W(3,:)/W(1,:)
    U(4,:) = 0.0d0
    U(5,:) = (gamma-1.0d0)*( W(5,:)-U(1,:)*( U(2,:)**2 + U(3,:)**2)/2.0d0 )  
        
    U_SA = W_SA/U(1,:)
    where( U_SA <= 0.0d0 ) 
        U_SA = 1.0E-10
    end where
      
    chi_SA = U_SA/( muL/U(1,:) ) 
    
    muT =  chi_SA**3/( chi_SA**3 + Cv1_SA**3 )* U(1,:)*U_SA
    
    deallocate( dw1 )
    deallocate( dwn )
    
    deallocate( dWT1 )
    deallocate( dWTn )
    
end subroutine
        
        
        
        
        