subroutine conFlux 
    !*******************************************************************
    !   Description: use Roe scheme to calculate the convective flux.
    !	Date: 2016-4-28
    !   Modification: 
    !*******************************************************************
    implicit none
    integer::i,j
    integer::ncl,ncr
    
    real(8),allocatable::UL(:,:),UR(:,:)                     ! rou, u, v, w, p of the left and right state of each edge
   
    real(8)::ux,uy,Vn,ut,vt,Vnt
    real(8)::nx,ny
    real(8)::c_av
   
    real(8)::flux(5)
    
    !---------------------------------------
    allocate(UL(5,nedges))  
    allocate(UR(5,nedges))
    
    call reconstruct_BJ(UL,UR)
    
    !calculathe  convective flux.
    Fc = 0.0d0 
    lamda_c = 0.0d0
    lamda_v = 0.0d0
    
    do i=1,nedges
        ncl=iedge(3,i)
        ncr=iedge(4,i)
        
        ut =  0.5d0 * ( U_Rot(1,iedge(1,i)) + U_Rot(1,iedge(2,i)) )
        vt =  0.5d0 * ( U_Rot(2,iedge(1,i)) + U_Rot(2,iedge(2,i)) )
        
        Vnt = ut* vector(1,i)   + vt *vector(2,i)
        
        ux =  U_av(2,i) - ut
        uy =  U_av(3,i) - vt
        
        Vn = ux * vector(1,i)   + uy * vector(2,i)
       
        if( ncr < 0) then  
            Fc(1,ncl) = Fc(1,ncl) + U_av(1,i)*Vn
            Fc(2,ncl) = Fc(2,ncl) + U_av(1,i)*U_av(2,i)*Vn + U_av(5,i)*vector(1,i)
            Fc(3,ncl) = Fc(3,ncl) + U_av(1,i)*U_av(3,i)*Vn + U_av(5,i)*vector(2,i)
            Fc(4,ncl) = 0.0d0
            Fc(5,ncl) = Fc(5,ncl) + U_av(1,i)*Vn*( U_av(5,i)/U_av(1,i)*gamma/(gamma-1.0d0) + (U_av(2,i)**2+U_av(3,i)**2)/2.0d0 ) 
            Fc(5,ncl) = Fc(5,ncl) + Vnt*U_av(5,i)  
            
            !H= U(5)/U(1)*gamma / (gamma-1.0) + ( U(2)**2 + U(3)**2 ) /2.0  
        else    
            call flux_Roe( UL(:,i),UR(:,i),i,flux)
            ! the convective flux, ledt plus and right subtract
            Fc(:,ncl) = Fc(:,ncl) + flux  * ds(i)
            Fc(:,ncr) = Fc(:,ncr) - flux  * ds(i)
        
        end if
        
        c_av=sqrt( U_av(5,i)*gamma/U_av(1,i) )
        
        alf_c(i)= abs(Vn) + c_av * ds(i)
        alf_v(i)= max( 4.0/3/U_av(1,i),gamma/U_av(1,i) ) *( muL_av(i)/PrL+muT_av(i)/PrT )
        
        lamda_c(ncl)  = lamda_c(ncl) + alf_c(i)
        lamda_v(ncl)  = lamda_v(ncl) + alf_v(i)*ds(i)**2/vol(ncl)
        if(ncr > 0) then
            lamda_c(ncr)  = lamda_c(ncr) + alf_c(i)
            lamda_v(ncr)  = lamda_v(ncr) + alf_v(i)*ds(i)**2/vol(ncr)
        end if
      
    end do
    
    !dt = CFL*vol/lamda_c   
    
    deallocate(UL)  
    deallocate(UR)
    
end subroutine


    


 
