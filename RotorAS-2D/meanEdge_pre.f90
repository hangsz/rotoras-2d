subroutine meanEdge_pre  
    !*******************************************************************
    !   Description: get the flow properties on each edge
    !	Date: 2016-4-28
    !   Modification: 
    !*******************************************************************
    
    implicit none
    integer::i
    integer::ncl,ncr
    real(8)::nx,ny
    real(8)::ux,uy,Vn                   ! the flow speed cross each edge
    real(8)::Ma                         ! mach number on any edge 
    
    U_av = 0.0                                    
    
    do i=1,nedges
        ncl = iedge(3,i)                !get the number of the left and the right cell
        ncr = iedge(4,i)
        
        select case( ncr ) 
        case (-1)                       ! -1 represents that the edge is the wall, using wall boundary conditions
          
            U_av(1,i) = U(1,ncl)        ! drou/dy = 0, normal gradient of density is zero
            U_av(2,i) = 0.5d0 * ( U_Rot(1,iedge(1,i)) + U_Rot(1,iedge(2,i)) )                 ! no slipping condition
            U_av(3,i) = 0.5d0 * ( U_Rot(2,iedge(1,i)) + U_Rot(2,iedge(2,i)) )   
            U_av(4,i) = 0.0d0
            U_av(5,i) = U(5,ncl)        ! dP/dt = 0, normal gradient of pressure is zero
            
            !U_av(6,i) = U_av(5,i)/R/U_av(1,i)
            
        case (-2)                           ! -2 represents that the edge is the farfield  boundaries, using farfield boundary conditions
            
            nx = vector(1,i)/ds(i)
            ny = vector(2,i)/ds(i)
            ! the real speed of the flow crossing the edge has to subtract the grid speed from the folw speed
            ux =  U(2,ncl) - 0.5d0 * ( U_Rot(1,iedge(1,i)) + U_Rot(1,iedge(2,i)) )
            uy =  U(3,ncl) - 0.5d0 * ( U_Rot(2,iedge(1,i)) + U_Rot(2,iedge(2,i)) )
            Vn = ux * nx + uy * ny
            
            ! Temporarily, the edge state is asummed to be equal to the ncl cell state
            Ma = abs(Vn) / sqrt( gamma*U(5,ncl)/U(1,ncl) )
            
            if( Vn <= 0.0d0) then         ! inflow  
                if( Ma >= 1.0d0)  then    
                    ! supersonic 
                    ! convecition dominated, flow properities on the inflow boundaries are determined by the inflow 
                    U_av(1,i) = rou_inf
                    U_av(2,i) = u_inf
                    U_av(3,i) = v_inf
                    U_av(4,i) = 0.0d0
                    U_av(5,i) = p_inf  
                    
                    !U_av(6,i) = U_av(5,i)/R/U_av(1,i)
       
                else                     
                    ! subsonnic
                    ! convection does't dominated, flow is infuenced by both the upwind flow and the downwind flow
                    U_av(1,i) = rou_inf
                    U_av(2,i) = u_inf
                    U_av(3,i) = v_inf
                    U_av(4,i) = 0.0d0
                    U_av(5,i) = U(5,ncl)
                    !U_av(6,i) = p_inf/R/rou_inf  
                    !U_av(1,i) =  U_av(5,i)/R/U_av(6,i)
                     
                end if
                
            else    
                ! outflow
                if(Ma >= 1.0d0)  then    
                    ! supersonic
                    ! convection dominated, flow propertities on the outer boundaries are determined by the outflow
                    U_av(1,i) = U(1,ncl)
                    U_av(2,i) = U(2,ncl)
                    U_av(3,i) = U(3,ncl)
                    U_av(4,i) = 0.0d0
                    U_av(5,i) = U(5,ncl) 
                    
                    !U_av(6,i) = U_av(5,i)/R/U_av(1,i) 
                else  
                    U_av(1,i) = U(1,ncl)
                    U_av(2,i) = U(2,ncl)
                    U_av(3,i) = U(3,ncl)
                    U_av(4,i) = 0.0d0
                    U_av(5,i) = p_inf 
                    !U_av(6,i) = U(5,ncl)/R/U(1,ncl)
                    !U_av(1,i) =  U_av(5,i)/R/U_av(6,i)
                    
                end if
                
            end if
            
        case default       ! folw properities are averaged on the edge in the internal domain        
           
            U_av(1,i)=0.5d0*( U(1,ncl) + U(1,ncr) )    
            U_av(2,i)=0.5d0*( U(2,ncl) + U(2,ncr) )
            U_av(3,i)=0.5d0*( U(3,ncl) + U(3,ncr) )
            U_av(4,i)=0.0d0
            U_av(5,i)=0.5d0*( U(5,ncl) + U(5,ncr) )
            
            !U_av(6,i) =  U_av(5,i)/R/U_av(1,i)
            
        end select
       
        ! temperature are calculated by the ideal gas law
        U_av(6,i)=U_av(5,i)/R/U_av(1,i)
        
    end do
    
    muL =   1.45d0*( U(5,:)/R/U(1,:) )**1.5d0/ ( U(5,:)/R/U(1,:) + 110.0d0 ) * 1.0E-6
    muL_av = 1.45d0* U_av(6,:)**1.5d0/( U_av(6,:)+110.0d0 ) *1.0E-6
    
    
end subroutine