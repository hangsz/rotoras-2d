    !*******************************************************************
    !	Copyright(c), 2016-2026, Sun Zhenhang
    !   All rights reserved.
    !	
    !	File name: FVM
    !	Version: 2.0
    !	Author:
    !	Date: 2016-4-28
    !
    !   Description: finite volume method 
    !	
    !*******************************************************************
     
module FVM              
    use controlDict
    use gridInfo
    use SA        
    use SST
    use motion
    use precondition

    implicit none   
    real(8)::u_inf,v_inf,a_inf    !incoming flow's velocities and sound velocity

    real(8),allocatable::U(:,:) , U_av(:,:) ,&        !the oringinal variables of every cell
                        W(:,:)   , &                 !the conservative variables of every cell   
                        Fc(:,:)  , &                 !convective flux of every cell
                        !alf(:),&
                        alf_c(:) ,alf_v(:), &        !spectral radius of every edge
                        lamda_c(:) ,lamda_v(:), &    !spectral radius of every edge
                        Dissi(:,:)  , &              !artificial dissipation of every cell
                        dt(:)      ,&                !time step of every cell 
                        Grad(:,:,:)  ,&
                        Fv(:,:), &
                        Rsi(:,:) , &                 !residual 
                        muL(:), muL_av(:) 

    !-----------------
    ! dual time
    integer,save:: isdiverge = 0                    ! 0 not diverge
    integer,save:: currentPhase = 0,endPhase = 0
    real(8),save:: t_total=0.0d0

    real(8),allocatable::Wn(:,:),Wn1(:,:),QW(:,:) 
    !--------------
    real(8),allocatable::muT(:),muT_av(:)

    integer,allocatable::assigned(:)

    integer,allocatable::lower(:,:),upper(:,:)              !this two arrays are needed for LU-SGS method 

    !--------------------------
    real(8),allocatable::U_Rot(:,:)

    !----------------------------------------------------
    ! variable specification

    ! u_inf,v_inf,a_inf          :      incoming flow's velocities and sound velocity, m/s
    ! U,W                        :      the primative variables and the conservative variables of the N-S equations of each cell.
    ! U_av                       :      the primative varibales of each edge
    ! U(1,:)    - density, kg/m^3
    ! U(2:3,:)  - the x-component and y-component of the velocity, m/s
    ! U(4,:)    - the alternative z-component of the velocity, m/s; it is no use in 2-D. 
    ! U(5,:)    - the static pressure, pa
    ! U(6,:)    - the temperature, K

    ! Wn,Wn1                     :      the intermediate conservative variables, it's used in dual time algorithm.
    ! QW                         :      the source term of dual time algorithm.

    ! Fc,Fv                      :      the convective and the viscous flux of the flow crossing the edge of each cell,using boundary flow properties. 
    ! Dissi                      :      the artificial dissipation of each cell.
    ! Rsi                        :      the residual

    ! alf_c,alf_v                :      the convective and visous spectral radius of each edge.
    ! lamda_c,lamda_v            :      the total of alf_c and alf_v respectively of each cell.
    ! dt                         :      the local time step of each cell.
    ! muL,muL_av                 :      the molecule viscosity of each cell and each edge respectively.

    ! Grad                       :      the gradient of the variables of each cell.

    ! assigned                   :      stores the reorderd sequence of the cell number.
   
    ! lower,upper                :      stores the lower cells and upper cells of each cell.
   
    ! U_Rot                      :      rotating speed of each node.
    !-----------------------------------------------------


contains
    !----------------------------------
    include "flowInit.f90"
    include "flowInit_static.f90"
    include "flowInit_dynamic.f90"
    !---------------------------------------
    ! calculate the properties on the edge, considering the boundary conditions
    include "meanEdge.f90"
    include "meanEdge_pre.f90"
    include "meanEdge_1.f90"
    !----------------------------------------

    ! convective flux
    ! JST scheme
    include "conFlux_JST.f90"
    include "artDissipation.f90"

    !--------
    ! upwind scheme
    include "reconstruct_BJ.f90"
    include "flux_Roe.f90"
    include "conFlux.f90"
    !-----------------------------------------
    ! viscous flux
    include "gradient.f90"     
    include "visFlux.f90"

    !--------------------------------
    ! solver
    include "solver.f90"
    !---------------------------------
    ! algebra equation solver
    include "reorder.f90"
    include "LU_SGS.f90"
    include "LU_SGS_SA.f90"
    include "LU_SGS_SST.f90"

    !-------------------------------
    ! post process
    include "outputFreeFlow.f90"
    include "output.f90"
    include "outResi.f90"
    !-------------------------------

subroutine allocateMemory
    !*******************************************************************
    !   Description: 
    !	Date: 2016-4-28
    !   Modification: 
    !******************************************************************* 

    write(*,*)  "allocateMemory"

    !allocate memory 
    allocate( U(5,ncells) )        !the primative variables            

    allocate( W(5,ncells) )        !the conserved variables
    !---------------------------
    ! dual time
    allocate( U_Rot(2,nnodes ) )
    allocate( Wn(5,ncells) )         !the conserved variables at n step
    allocate( Wn1(5,ncells) )        !the conserved variables at n+1 step
    allocate( QW(5,ncells) )        

    !-------------------------
    allocate( U_av(6,nedges) )     ! the primative variable on the edges, the boundary conditon is used here 
    allocate( Grad(2,6,ncells) )   ! the gradient of every cell,rou,u,v,w,p,T
    allocate( Fc(5,ncells) )       ! the convective flux
    allocate( Dissi(5,ncells) )    ! the artificial dissipation

    !viscosity
    allocate( Fv(5,ncells) )       !the  viscous flux
    allocate( muL(ncells) )        ! molecular viscosity
    allocate( muL_av(nedges) )     

    allocate( muT(ncells) )        !the turbulent viscosity
    allocate( muT_av(nedges) )
   

    !allocate( alf(nedges) )        !the spectrum radius of every edge
    allocate( alf_c(nedges) )       !the spectrum radius of every edge
    allocate( alf_v(nedges) )
    allocate( lamda_c(ncells) )     ! sum of spectrum radius of each cell
    allocate( lamda_v(ncells))
    allocate( dt(ncells) )          ! the time  step

    allocate( Rsi(5,ncells) )       !the residual

    !LU-SGS
    allocate( assigned(ncells) )    ! the sequcence of the cells being reordered
    allocate( lower(4,ncells) )     ! the lower edges of every cell
    allocate( upper(4,ncells) )     ! the upper edges of every cell

end subroutine

subroutine deallocateMemory
    !*******************************************************************
    !   Description: 
    !	Date: 2016-4-28
    !   Modification: 
    !******************************************************************* 

    write(*,*)  "deallocateMemory"

    !allocate memory 
    deallocate( U )        !the primative variables            
    deallocate( W )        !the conserved variables
    !---------------------------

    deallocate( U_Rot )

    ! dual time
    deallocate( Wn )        !the conserved variables
    deallocate( Wn1 )       !the conserved variables
    deallocate( QW )        

    !-------------------------
    deallocate( U_av )     !the primative variable on the edges, the boundary conditon is used here 
    deallocate( Grad )   !the gradient of every cell,rou,u,v,w,p,T
    deallocate( Fc)       !the convective flux
    deallocate( Dissi )   ! the artificial dissipation

    !viscosity
    deallocate( Fv )       !the viscous flux
    deallocate( muL)
    deallocate( muL_av )     

    deallocate( muT ) 
    deallocate( muT_av )
    !time
    deallocate( alf_c )        !the spectrum radius of every edge
    deallocate( alf_v )
    deallocate( lamda_c )
    deallocate( lamda_v)
    deallocate( dt )         ! the time  step

    deallocate( Rsi )      !the residual

    !LU-SGS
    deallocate( assigned )   !the sequcence of the cells being reordered
    deallocate( lower )    !the lower edges of every cell,triangle has three edges
    deallocate( upper )    !the upper edges of every cell


    call deallocateMemory_Grid

    select case(turModelNum)
    case (1)  
        call deallocateMemory_SA
    case (2)  
        call deallocateMemory_SST
    end select

    if(moveNUm /=0)   call deallocateMemory_Moving
    if( isPrecondition ==1 )   call deallocateMemory_Pre

end subroutine


subroutine converge(flag,oldRsi,newRsi) 
    !*******************************************************************
    !   Description: judge wheather the calculation has converged
    !	Date: 2016-4-28
    !   Modification: 
    !******************************************************************* 
    implicit none

    integer,intent(out)::flag                  !flag, 1:converge;0:disconverge
    real(8),intent(in)::oldRsi(:),newRsi(:) 

    integer,save::oriFlag = 1
    real(8),save::oriRsi(5)
    real(8)::tempRsi(5)

    !write(*,*)  "Converge"
    if( moveNum==0)  then
        if(oriFlag<=3)  then 
            oriRsi = oldRsi
            !oriRsi(1) = rou_inf
            !oriRsi(2) = abs(u_inf)
            !oriRsi(3) = abs(v_inf)
            !oriRsi(5) = p_inf
            oriFlag = oriFLag + 1   
        end if
    else
        oriRsi = oldRsi
    end if

    flag = 0

    tempRsi = newRsi/oldRsi

    if (tempRsi(1) < eps_in) flag = 1  ! inner iteration

    if(moveNum==0) then
        if ( tempRsi(1) < eps) flag = 2   ! static inner iteration
    end if

    
    write(*,*)  "muT:",muT(100)
    write(*,*)  "        value(100)          inner-residual           residual "
    write(*,*)  "rou:", U(1,100), tempRsi(1), newRsi(1)/oriRsi(1)
    write(*,*)  "u  :", U(2,100), tempRsi(2), newRsi(2)/oriRsi(2)
    write(*,*)  "v  :", U(3,100), tempRsi(3), newRsi(3)/oriRsi(3)
    write(*,*)  "p  :", U(5,100), tempRsi(5), newRsi(5)/oriRsi(5)


    write(*,*)  


end subroutine

subroutine diverge        
    !*******************************************************************
    !   Description:   judge wheather the calculation has diverged
    !	Date: 2016-4-28
    !   Modification: 
    !******************************************************************* 
    implicit none

    integer::i,j
    !write(*,*)  "diverge"
    !---------------------------------
    do i=1,ncells
        do j=1,5
            if( isnan(U(j,i))) then
                write(*,*)
                write(*,*) "!!! Diverge !!!"
                write(*,*) "rou:", U(1,i)
                write(*,*) "u  :", U(2,i)
                write(*,*) "v  :", U(3,i)
                write(*,*) "p  :", U(5,i)
                write(*,*) "-------------"

                isDiverge = 1  ! diverge
                return
            end if   
        end do   
    end do

end subroutine

end module
