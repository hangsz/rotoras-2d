subroutine LU_SGS 
    !*******************************************************************
    !   Description: Lower-Upper Symmetric Successive Overrelexation scheme, get W(n+1)  at every time step
    !	Date: 2016-4-28
    !   Modification: 
    !******************************************************************* 
  
    implicit none
    
    integer::i,j,k
    integer(8)::edge,cell_num,cell_lower,cell_upper
    
    real(8)::sign
    real(8)::ut,vt,Vnt
    real(8)::ux,uy,Vn,c,rA_v,rA
    real(8)::U_temp(5),W_temp(5),Fc_n1(5),Fc_n(5),FcTemp(5)
    real(8)::dFc(5)
    real(8)::diag(5)
    real(8),allocatable::dw1(:,:)
    real(8),allocatable::dwn(:,:)
    
    integer::error
    !-----------------------------------------
    !write(*,*)  "LU-SGS"
    allocate( dw1(5,ncells),stat = error)
    if( error /= 0) write(*,*) "dw1 allocate Error"
    
    allocate( dwn(5,ncells),stat = error ) 
    if( error /= 0) write(*,*) "dwn allocate Error"
    
    !-------------------------------------------------------------------------------
    dw1 = 0.0d0
    dwn = 0.0d0

    !Forward  sweep
    do i=1,ncells   !it doesn't mean that the sequence is identical to the cell order
        
        cell_num = assigned(i)  
          
        dFc = 0.0d0  
         
        do j=1,4 
            edge = lower( j,cell_num )   !each cell has three lower edges totally(in fact,it can only have two lower or upper edges at most)   
            if( edge == 0)  exit
           
            ! determine the cell number lower than the current cell
            ! because only the lower edge is stored , it has to be tested which cell is the lower cell
            if( iedge(3,edge) /= cell_num ) then
                cell_lower = iedge(3,edge)
                sign =  -1.0d0
            else
                cell_lower = iedge(4,edge)
                sign = 1.0d0
            end if
            !-------------------------------------
            !convective flux, calculate  the lower cells's n+1 step convective flux 
            W_temp = W(:,cell_lower) + dw1(:,cell_lower)  
            
            U_temp(1) = W_temp(1)
            U_temp(2) = W_temp(2)/W_temp(1)
            U_temp(3) = W_temp(3)/W_temp(1)
            U_temp(4) = 0.0d0
            U_temp(5) = (gamma-1.0)*( W_temp(5)-U_temp(1)*( U_temp(2)**2 + U_temp(3)**2)/2.0d0 )  
            
            if( icell(3,cell_lower)/=icell(4,cell_lower) ) then
                ut = 0.25d0 * sum( U_Rot(1,icell(:,cell_lower)) )
                vt = 0.25d0 * sum( U_Rot(2,icell(:,cell_lower)) )
            else
                ut = 1.0d0/3.0d0 * sum( U_Rot(1,icell(1:3,cell_lower)) )
                vt = 1.0d0/3.0d0 * sum( U_Rot(2,icell(1:3,cell_lower)) )
            end if
            
            Vnt = ut*vector(1,edge) + vt*vector(2,edge)
            
            ux =  U_temp(2) - ut
            uy =  U_temp(3) - vt
            
            Vn = ux * vector(1,edge)   + uy * vector(2,edge) 
            
            Fc_n1(1) = W_temp(1) * Vn
            Fc_n1(2) = W_temp(2) * Vn + vector(1,edge) * U_temp(5)
            Fc_n1(3) = W_temp(3) * Vn + vector(2,edge) * U_temp(5)
            Fc_n1(4) = 0.0d0
            Fc_n1(5) = ( W_temp(5) + U_temp(5) ) * Vn
            Fc_n1(5) = Fc_n1(5) + Vnt*U_temp(5)
            
            
            !calculate  the lower cells's n step convective flux 
            ux =  U(2,cell_lower) - ut
            uy =  U(3,cell_lower) - vt
            Vn = ux * vector(1,edge)   + uy * vector(2,edge)
            
            Fc_n(1) = W(1,cell_lower) * Vn
            Fc_n(2) = W(2,cell_lower) * Vn + vector(1,edge) * U(5,cell_lower)
            Fc_n(3) = W(3,cell_lower) * Vn + vector(2,edge) * U(5,cell_lower)
            Fc_n(4) = 0.0d0
            Fc_n(5) = ( W(5,cell_lower) + U(5,cell_lower) )* Vn
            Fc_n(5) = Fc_n(5) + Vnt * U(5,cell_lower)
            
            !-----------------------------------------------
            ! the convective flux has to modify
            if( isPrecondition == 1 ) then
                FcTemp = Fc_n1
                do k=1,5
                    Fc_n1(k) = dot_product(MInvSP(k,:,cell_lower),FcTemp )
                end do
                FcTemp = Fc_n
                do k=1,5
                    Fc_n(k) = dot_product( MInvSP(k,:,cell_lower),FcTemp )
                end do
                
                rA = omg*SPalf_c(edge) + SPalf_v(edge)*ds(edge)/lij(edge)
            else
                rA = omg*alf_c(edge) + alf_v(edge)*ds(edge)/lij(edge)
            end if
            
            !-----------------------------------------------------
            dFc = dFc + sign*(Fc_n1 - Fc_n ) -  rA*dw1(:,cell_lower)  
            
        end do 
        
        if( isPrecondition == 1 ) then
            diag = vol(cell_num)/dt(cell_num) + omg/2.0d0*SPlamda_c(cell_num) + SPlamda_v(cell_num)
        else
            diag = vol(cell_num)*( 1.0d0/dt(cell_num) + 1.5d0/dt_r ) + omg/2.0*lamda_c(cell_num) + lamda_v(cell_num)  
        end if
        
        dw1(:,cell_num) = 1.0d0/diag * ( - Rsi(:,cell_num) - 0.5d0*dFc )
        
    end do    
    
    !Backward sweep
    do i = ncells,1,-1
        
        cell_num = assigned(i)    
        
        dFc = 0.0d0
    
        do j=1,4
            
            edge = upper( j,cell_num )
            
            if( edge == 0 )  exit
                
            if( iedge(3,edge) /= cell_num ) then
                cell_upper = iedge(3,edge)
                sign = -1.0d0
            else
                cell_upper = iedge(4,edge)
                sign = 1.0d0
            end if
                         
            !-------------------------------------------------
            !calculate the upper cells's n+1 convective flux 
            W_temp = W(:,cell_upper) + dwn(:,cell_upper)  
          
            U_temp(1) = W_temp(1)
            U_temp(2) = W_temp(2)/W_temp(1)
            U_temp(3) = W_temp(3)/W_temp(1)
            U_temp(4) = 0.0d0
            U_temp(5) = (gamma-1.0d0)*( W_temp(5)-U_temp(1)*( U_temp(2)**2 + U_temp(3)**2)/2.0d0 )  
            
            if( icell(3,cell_upper)/=icell(4,cell_upper) ) then
                ut = 0.25d0 * sum( U_Rot(1,icell(:,cell_upper)) )
                vt = 0.25d0 * sum( U_Rot(2,icell(:,cell_upper)) )
            else
                ut = 1.0d0/3.0d0 * sum( U_Rot(1,icell(1:3,cell_upper)) )
                vt = 1.0d0/3.0d0 * sum( U_Rot(2,icell(1:3,cell_upper)) )
            end if
            Vnt = ut * vector(1,edge)   + vt * vector(2,edge)
            
            ux =  U_temp(2) - ut
            uy =  U_temp(3) - vt  
            Vn = ux * vector(1,edge)   + uy * vector(2,edge) 
         
            
            Fc_n1(1) = W_temp(1) * Vn
            Fc_n1(2) = W_temp(2) * Vn + vector(1,edge) *U_temp(5)
            Fc_n1(3) = W_temp(3) * Vn + vector(2,edge) *U_temp(5)
            Fc_n1(4) = 0.0d0
            Fc_n1(5) = ( W_temp(5) + U_temp(5) ) * Vn
            Fc_n1(5) = Fc_n1(5) + Vnt*U_temp(5)
            
            !calculate  the upper cells'S_SA n step convective flux 
            ux =  U(2,cell_upper) - ut
            uy =  U(3,cell_upper) - vt
            Vn = ux * vector(1,edge)   + uy * vector(2,edge)
            
            Fc_n(1) = W(1,cell_upper) * Vn
            Fc_n(2) = W(2,cell_upper) * Vn + vector(1,edge) *U(5,cell_upper)
            Fc_n(3) = W(3,cell_upper) * Vn + vector(2,edge) *U(5,cell_upper)
            Fc_n(4) = 0.0d0
            Fc_n(5) = ( W(5,cell_upper) + U(5,cell_upper) )* Vn
            Fc_n(5) = Fc_n(5) +  Vnt*U(5,cell_upper) 
            
            !-----------------------------------------------
            ! the convective flux has to modify
            if( isPrecondition==1) then
                FcTemp = Fc_n1
                do k=1,5
                    Fc_n1(k) = dot_product( MInvSP(k,:,cell_upper),FcTemp )
                end do
                FcTemp = Fc_n
                do k=1,5
                    Fc_n(k) = dot_product( MInvSP(k,:,cell_upper),FcTemp )
                end do
                rA = omg*SPalf_c(edge) + SPalf_v(edge)*ds(edge)/lij(edge)
            else
                rA = omg*alf_c(edge) + alf_v(edge)*ds(edge)/lij(edge)
            end if
    
            !--------------------------------------------------
            dFc = dFc + sign*( Fc_n1 - Fc_n ) - rA*dwn(:,cell_upper)        
              
        end do
        
        if( isPrecondition==1) then
            diag = vol(cell_num)/dt(cell_num)  + omg/2.0d0*SPlamda_c(cell_num) + SPlamda_v(cell_num)
        else 
            diag = vol(cell_num)*( 1.0d0/dt(cell_num) + 1.5d0/dt_r ) + omg/2.0d0*lamda_c(cell_num) + lamda_v(cell_num)
        end if
        
        dwn(:,cell_num) = dw1(:,cell_num) -  0.5d0*dFc/diag
            
    end do
          
    W = W + dwn 
 
    !calculate the original variables
    
    U(1,:) = W(1,:)
    U(2,:) = W(2,:)/W(1,:)
    U(3,:) = W(3,:)/W(1,:)
    U(4,:) = 0.0d0
    U(5,:) = (gamma-1.0d0)*( W(5,:)-U(1,:)*( U(2,:)**2 + U(3,:)**2)/2.0d0 )  
    
    do i=1,ncells
        do j=1,5
            if( isnan(U(j,i))) stop" !!! Diverge !!!"
        end do
    end do
    
    deallocate( dw1 )
    deallocate( dwn )
    
end subroutine
        
        
        
        
        