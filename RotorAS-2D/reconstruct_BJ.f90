subroutine reconstruct_BJ(UL,UR)
    !*******************************************************************
    !   Description: get the primative variables of left and right state
    !	Date: 2016-4-28
    !   Modification: 
    !*******************************************************************

    implicit none
    real(8),intent(out)::UL(:,:),UR(:,:)
    integer::i,j
    integer::ncl,ncr
    
    real(8)::epsi2L,epsi2R,dhL,dhR
    real(8)::d1maxL(5),d1minL(5),d1maxR(5),d1minR(5)
   
    real(8)::ux,uy,Vn,ut,vt,Vnt                
    real(8)::d2L(5),d2R(5)
    real(8)::psiL(5),psiR(5)
    real(8)::nx,ny
   
    
    real(8),allocatable::Umax(:,:),Umin(:,:),psi(:,:)   

    !Venkatakrishnan's limiter  for Roe scheme
    allocate(Umax(5,ncells))  
    allocate(Umin(5,ncells))
    allocate(psi(5,ncells))
    
    !------------------------------------------------
    !calculathe the Umax Umin
    Umax= U
    Umin= U
    
    do i=1,nedges        !traverse along the edge
        
        ncl=iedge(3,i)
        ncr=iedge(4,i)
        
        do j=1,5  
            Umax(j,ncl) = max( Umax(j,ncl),U(j,ncl) )
            Umin(j,ncl) = min( Umin(j,ncl),U(j,ncl) )
        end do
        if ( ncr > 0)  then  !  right cell exist
            
            !the maximun/minimum value is among the last Umax/Umin and the current left and right values of each cell
            do j=1,5
                Umax(j,ncr) = max( Umax(j,ncr),U(j,ncl),U(j,ncr) )
                Umin(j,ncr) = min( Umin(j,ncr),U(j,ncl),U(j,ncr) ) 
            end do
        end if  
    end do
    
    !----------------------------------------------------------
    !Venkatakrishnan's limiter
    !notice : all quantities must be non-dimensional
    
    psi = 1.0d0   ! limited factor--psi is no more than 1
     
    do i=1,nedges
        
        ncl=iedge(3,i)
        ncr=iedge(4,i)
     
        if( ncr > 0 ) then
            d1maxL =  Umax(:,ncl) - U(:,ncl) 
            d1minL =  Umin(:,ncl) - U(:,ncl)
            
            d1maxR =  Umax(:,ncr) - U(:,ncr)
            d1minR =  Umin(:,ncr) - U(:,ncr)
         
            dhL =sqrt( vol(ncl) )
            dhR =sqrt( vol(ncr) )
            
            epsi2L = ( K*dhL )**3
            epsi2R = ( K*dhR )**3
            
            do j=1,5   
                ! calculate psi of the left state on each edge.
                d2L(j) = dot_product( Grad(:,j,ncl),rL(:,i) ) 
                d2R(j) = dot_product( Grad(:,j,ncr),rR(:,i) )
                
                d2L(j) =sign( abs(d2L(j)) + 1.0E-12 , d2L(j) )
                d2R(j) =sign( abs(d2R(j)) + 1.0E-12 , d2R(j) )
                
                if ( d2L(j) > 0.0d0 ) then
                    psiL(j) = 1.0d0/d2L(j)* ( (d1maxL(j)**2 + epsi2L)*d2L(j) + 2.0d0 * d2L(j)**2 * d1maxL(j) ) / ( d1maxL(j)**2 + 2.0d0*d2L(j)**2 + d1maxL(j)*d2L(j) + epsi2L )
                else if( d2L(j) < 0.0d0 ) then
                    psiL(j) = 1.0d0/d2L(j)* ( (d1minL(j)**2 + epsi2L)*d2L(j) + 2.0d0 * d2L(j)**2 * d1minL(j) ) / ( d1minL(j)**2 + 2.0d0*d2L(j)**2 + d1minL(j)*d2L(j) + epsi2L )   
                else
                    psiL(j) = 1.0d0
                end if
                
                ! calculate psi of the right state on each edge.
                if ( d2R(j) > 0.0d0 ) then
                    psiR(j) = 1.0d0/d2R(j)* ( (d1maxR(j)**2 + epsi2R)*d2R(j) + 2.0d0 * d2R(j)**2 * d1maxR(j) ) / ( d1maxR(j)**2 + 2.0d0*d2R(j)**2 + d1maxR(j)*d2R(j) + epsi2R )
                else if( d2R(j) < 0.0d0 ) then
                    psiR(j) = 1.0d0/d2R(j)* ( (d1minR(j)**2 + epsi2R)*d2R(j) + 2.0d0 * d2R(j)**2 * d1minR(j) ) / ( d1minR(j)**2 + 2.0d0*d2R(j)**2 + d1minR(j)*d2R(j) + epsi2R )   
                else 
                    psiR(j) = 1.0d0
                end if
            
                psi(j,ncl)  = min( psi(j,ncl),psiL(j),psiR(j) )
                psi(j,ncr)  = min( psi(j,ncr),psiL(j),psiR(j) )  
       
               ! test wheather the psi of each cell is lower than 1
               ! if( psi(j,ncl) < 0.0  .OR. psi(j,ncl) >1.0 ) then
               !     stop "1"
               ! else
               !     write(*,*)  psi(j,ncl)
               ! end if
               ! 
               ! if( psi(j,ncr) < 0.0  .OR. psi(j,ncr) >1.0 ) then
               !     stop "2"
               ! else
               !     write(*,*)  psi(j,ncr)
               !end if
            end do
        end if
         
    end do
    
    do i=1,nedges
        ncl = iedge(3,i)
        ncr = iedge(4,i)
        if( ncr > 0) then
            do j=1,5
                UL(j,i) = U(j,ncl) + psi(j,ncl) * dot_product( Grad(:,j,ncl),rL(:,i) )
                UR(j,i) = U(j,ncr) + psi(j,ncr) * dot_product( Grad(:,j,ncr),rR(:,i) )
            end do   
        end if
    end do
    
    deallocate(Umax)  
    deallocate(Umin)
    deallocate(psi )
      
end subroutine


    


 
