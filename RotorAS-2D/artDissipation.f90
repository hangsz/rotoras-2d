subroutine artDissipation(Palf_c,PinvM)             
    !*******************************************************************
    !   Description: artificial dissipation calculation
    !	Date: 2016-4-28
    !   Modification: 
    !*******************************************************************
    implicit none
    integer::i,j
    real(8),intent(in),optional::Palf_c(:),PInvM(:,:,:)
    
    integer::ncl,ncr
    real(8)::dis(5)                           !the arTificial dissipation of every edge
    real(8)::nu,epsi2,epsi4                    !the pressure sensor
    real(8),allocatable::d2W(:,:)                !the  median to calculate forth-order difference
    
    real(8),allocatable::preArt(:,:),disTemp(:)
    !----------------------------------------------
    
    !write(*,*) "artDissipation"
    allocate(d2W(5,ncells))
   
    Dissi = 0.0d0
    d2W = 0.0d0 
   
    !the first cycle to calculate the d2W
    do i = 1,nedges
        ncl = iedge(3,i)                      
        ncr = iedge(4,i)
        if( ncr > 0 )  then         !edge has no right cell is ignored
            !d2W(:,ncl) = d2W(:,ncl) + 0.5d0*( W(:,ncr) - W(:,ncl) )
            !d2W(:,ncr) = d2W(:,ncr) + 0.5d0*( W(:,ncl) - W(:,ncr) )
            d2W(:,ncl) = d2W(:,ncl) + ( W(:,ncr) - W(:,ncl) )
            d2W(:,ncr) = d2W(:,ncr) + ( W(:,ncl) - W(:,ncr) )
        end if
    end do
        
    !the second cycle to calculate the dissipation 
    
    if( .NOT. present(PinvM) ) then
        ! without preconditioning       
        do i = 1,nedges
            ncl = iedge(3,i)
            ncr = iedge(4,i)
            if( ncr > 0 )  then    !ncr less or equal to zero,the dissipation of this edge is set to zero
                nu = abs( U(5,ncr)-U(5,ncl) ) / abs( U(5,ncr)+U(5,ncl) ) 
                epsi2 = k2*nu
                epsi4 = max( 0.0d0 , k4-epsi2 )
                dis = alf_c(i)*epsi2*( W(:,ncr) - W(:,ncl) ) - alf_c(i)*epsi4*( d2W(:,ncr) - d2W(:,ncl) )
                !the left cell plus the dissipation and the right is contrary
                Dissi(:,ncl) = Dissi(:,ncl) + dis
                Dissi(:,ncr) = Dissi(:,ncr) - dis  
            end if
        end do  
    else
        ! with preconditioning
        
        allocate( preArt(5,5) )
        allocate( disTemp(5) )
        
        do i = 1,nedges
            ncl = iedge(3,i)
            ncr = iedge(4,i)
            if( ncr > 0 )  then    !ncr less or equal to zero,the dissipation of this edge is set to zero
                nu = abs( U(5,ncr)-U(5,ncl) ) / abs( U(5,ncr)+U(5,ncl) ) 
                epsi2 = k2*nu
                epsi4 = max( 0.0d0 , k4-epsi2 )
                dis = Palf_c(i)*epsi2*( W(:,ncr) - W(:,ncl) ) - Palf_c(i)*epsi4*( d2W(:,ncr) - d2W(:,ncl) )
              
                preArt = 0.5d0*( PInvM(:,:,ncl) + PInvM(:,:,ncr) )
                
                disTemp = dis
                do j=1,5
                    dis(j) =  dot_product( preArt(j,:),disTemp )
                end do
                
                Dissi(:,ncl) = Dissi(:,ncl) + dis
                Dissi(:,ncr) = Dissi(:,ncr) - dis  
                
            end if
        end do  
        
        
        deallocate( preArt )
        deallocate( disTemp )
        
    end if
       
    deallocate( d2W)   
    
end subroutine
