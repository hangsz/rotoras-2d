subroutine allocateMemory_SST
    !*******************************************************************
    !   Description:  
    !	Date: 2016-4-28
    !   Modification: 
    !*******************************************************************

    write(*,*) "allocateMemory_SST"
    
     
    allocate( U_SST(2,ncells) )
    allocate( U_av_SST(2,nedges) )
    
    allocate( W_SST(2,ncells) )
    
    !-----------
    ! dual time
   
    allocate( W_SST_n(2,ncells) )
    allocate( W_SST_n1(2,ncells) )
    allocate( QW_SST(2,ncells) )
   
    
    !-----------
   
    !allocate( dt(ncells) )         ! the time  step
    
    allocate( Fc_SST(2,ncells) ) 
    
    allocate( Grad_SST(2,2,ncells) )
    
    allocate( Fv_SST(2,ncells) ) 
    allocate( Dissi_SST(2,ncells) )
    allocate( Q_SST(2,ncells) )
    allocate( dQdW_SST(2,ncells) ) 
    
    allocate( Rsi_SST(2,ncells) )
    
     allocate( f2_SST(ncells) )       ! LU_SGS
    
    allocate( sigma_K_SST(ncells) )
    allocate( sigma_omg_SST(ncells) )
    allocate( Cw_SST(ncells) )
    allocate( beta_SST(ncells) )
    
    end subroutine
    
subroutine deallocateMemory_SST

    write(*,*) "deallocateMemory_SST"
    
    deallocate( U_SST )
    deallocate( U_av_SST )
    
    deallocate( W_SST )
    
    !-----------
    ! dual time
   
    deallocate( W_SST_n )
    deallocate( W_SST_n1 )
    deallocate( QW_SST )
    !-----------
   
    !deallocate( dt )         ! the time  step
    
    deallocate( Fc_SST ) 
    
    deallocate( Grad_SST )
    
    deallocate( Fv_SST ) 
    deallocate( Dissi_SST )
    deallocate( Q_SST )
    deallocate( dQdW_SST ) 
    
    deallocate( Rsi_SST )
    
    deallocate( f2_SST )       ! LU_SGS
    
    deallocate( sigma_K_SST )
    deallocate( sigma_omg_SST )
    deallocate( Cw_SST )
    deallocate( beta_SST )
    
end subroutine
