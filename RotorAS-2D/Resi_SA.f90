subroutine Resi_SA( U,U_av,alf_c,muL,muL_av ,Grad ,U_Rot)
    !*******************************************************************
    !   Description:  
    !	Date: 2016-4-28
    !   Modification: 
    !*******************************************************************
    implicit none
   
    integer::k
    real(8),intent(in)::U(:,:),U_av(:,:),alf_c(:),muL(:),muL_av(:),Grad(:,:,:),U_Rot(:,:)
    
    !write(*,*)   "Resi_SA"
    
    call meanEdge_SA( U_av,U_Rot)
    call gradient_SA( U_av )
    
    
    call conFlux_SA( U_av,U_Rot ) 
    
    if ( schemeType==2)  then
        call artDissipation_SA( U, alf_c )
    else
        Dissi_SA = 0.0d0 
    end if
    
    call visFlux_SA( U_av,muL_av )
                            
    !---------------------------------------
    !coefficients 
    
    !S_SA =  sqrt(2.0*Grad(1,2,:)**2 + (Grad(2,2,:)+Grad(1,3,:))**2 + 2.0*Grad(2,3,:)**2 ) - abs( Grad(2,2,:) - Grad(1,3,:) )
    !
    !S_SA =  abs(Grad(2,2,:)-Grad(1,3,:)) + Cprod_SA*min(0.0,S_SA ) !omg11 =0,omg22=0,omg12**2= omg21**2
    
    S_SA =  abs(Grad(2,2,:)-Grad(1,3,:))
    chi_SA = U_SA/( muL/U(1,:) ) 
    
    fv1_SA = chi_SA**3/( chi_SA**3 + Cv1_SA**3)
    fv2_SA = ( 1.0d0 + chi_SA/Cv2_SA)**(-3)    
    !chi_SA = max( 1.0E-3,chi_SA )
    fv3_SA = ( 1.0d0 + chi_SA*fv1_SA)*( 1.0d0 -fv2_SA)/ max( 1.0E-3,chi_SA )
    !----------------------------------------------
        
    S_SA = fv3_SA*S_SA + U_SA/( kapa_SA*d)**2 * fv2_SA
        
        
    rT_SA = U_SA / ( S_SA * kapa_SA**2 * d**2 ) 
    
    !do k=1,ncells
    !    write(*,*)  s_SA(k),rT_SA(k),d(k)
    !end do
    !
    !stop
    !    
    g_SA = rT_SA + Cw2_SA*( rT_SA**6 - rT_SA) 
        
    fw_SA = g_SA* (1.0d0 + Cw3_SA**6)**(1.0d0/6.0d0) /( g_SA**6 + Cw3_SA**6 )**(1.0d0/6.0d0)
        
    Q_SA = Cb1_SA*S_SA*W_SA + Cb2_SA/sigma_SA*( Grad_W_SA(1,:) * Grad_U_SA(1,:) + Grad_W_SA(2,:) * Grad_U_SA(2,:) ) - Cw1_SA * fw_SA * U(1,:)*(U_SA/d)**2
        
    !-----------------------------------------------------
      
    Rsi_SA= Fc_SA - Dissi_SA - Fv_SA  - Q_SA*vol
    
    !do k=1,100
    !    write(*,*)  S_SA(k),fw_SA(k),g_SA(k)
    !end do
    !
    !stop
    ! 
    !-----------------------------------------
    ! negative part is necessary to enhance the magnitude of the diaginal element , omit the generation part is necessary sometimes.
    !dQdW_SA =  Cb1_SA*S_SA  - 2.0*Cw1_SA*fw_SA*U_SA/d**2
    dQdW_SA = -2.0d0*Cw1_SA*fw_SA*U_SA/d**2
    
           
end subroutine


                       