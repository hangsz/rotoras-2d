module  controlDict   
    !*******************************************************************
    !	Copyright(c), 2016-2026, Sun Zhenhang
    !   All rights reserved.
    !	
    !	File name: controlDict
    !	Version: 2.0
    !	Author: Sun Zhenhang
    !	Date: 2016-4-28
    !
    !   Description: global parameters
    !	
    !   Modification: 
    !
    !*******************************************************************
    
    
    implicit none

    !-----------------------------------------------------------------------------
    !constant parameters
    real(8),parameter::pi = 3.1415926535898                
    !-------------------------------------------------------------------------------------------------
    !                             Inflow 
    !
    real(8),save::Ma_inf !=  0.6d0                !the mach number
    real(8),save::Re_inf !=  4.8E6                !the Re number

    real(8),save::att != 3.160d0                  !attack angle,��

    ! fix density and pressure(standard atmosphere, sea level 15�� ) 
    real(8),parameter::rou_inf = 1.225d0          ! density,kg/m^3 
    real(8),parameter::p_inf = 1.01325E5          ! pressure,N/m^2
    
    ! using value from standard atmosphere 
    real(8),save::muL_inf = 1.78E-5               ! molecular viscocity, pa

    real(8),parameter::C = 110.6d0                ! sutherland formula constant
    real(8),parameter::R = 287.0d0                ! the ideal gas constant,J/(kg*K)      
    real(8),parameter::gamma = 1.4d0              ! the specific heat ratio
    real(8),parameter::PrL=0.72d0                 ! Prantle coefficient
    real(8),parameter::PrT=0.9d0                  ! Turbulent Prantle coefficient 
    
    ! used to identify wheahter the flow is reverse
    integer,save :: isReverse              
                            ! 0. non-reverse flow
                            ! 1. reverse flow                        
    character(len=4),parameter::reverseDict(0:1) = ["-No","-Yes"]   ! used to write to screen or file
    
    !---------------------------------------------------------------------------------------!
    !                         static and dynamic                                            !
    integer,save::moveNum     
    !                        0. static
    !                        1. dynamic
    character(len=8),parameter::moveDict(0:1) = ["-static ","-dynamic"]
                                                                                  
    ! static                                                                                !
    integer,save::staticMax = 100           ! maximum iterative steps                       ! 
    ! dynamic                                                                               !
    integer,save::cycleNum = 3              ! three cycles                                  !       
    integer,save::cycleStep = 300           ! total steps every period                      !
    integer,save::steps = 5                 ! every steps, output to file                   ! 
    
    ! the phase to start from, reading its preceding phase
    integer,save:: startPhase 
    !                                                          
    real(8),parameter::rot_cen(2)=[0.25d0,0.0d0]   ! rotational center,(%,%)                !
    real(8),save::att_ampl != 4.59d0                ! rotational amplitude,��                 !
    real(8),save::kr != 0.0811d0                    ! reduced frequency                      !
    !---------------------------------------------------------------------------------------!
    
    !--------------------------------------------------------------------------------------------------
    !                            Discretasion 
    
    ! convective flux scheme                                                                !
    integer,save::conFluxNum                                                                !      
    !                   1.  JST scheme                                                      !       
    !                   2.  Roe scheme                                                      !
    character(len=4),parameter::conFluxDict(2) = ["-JST","-Roe"]          
    ! artifical dissipation, k2= 0.5~1, k4 = 1/256~1/32                                     !
    real(8),parameter::k2 = 0.6d0, k4 = 1.0d0/64.0d0                                        !                                   
    ! CFL number,implicit, 10��4~10��6     
    real(8),save::CFL=1.0E4                           
    ! time step, the times of viscous time step to convecitve time step 
    real(8),parameter::C_time = 2.0d0                                                       !
    ! Roe scheme, limiter                                                                   !
    real(8),parameter::K = 5.0d0                                                            !
    !---------------------------------------------------------------------------------------!      
    !                            Convergence                                                !                                                       
    real(8),save::eps = 1.0E-8         !outer iteration,  convergence precision             !
    
    ! dual time                                                                             !
    integer,save::inStep = 300           ! the dual time scheme iterative times             !
    real(8),save::eps_in=1.0E-3        !inner iteration,  convergence precision             !                                                                                       !
    ! real time step, the value is used only in static flow, auto computed in dynamic flow                         
    real(8),save::dt_r = 0.001d0    
    
   
    !LU-SGS,is used to calculate the D matrix                                               !
    real(8),parameter::omg = 1.5d0            !overrelaxation factor , 1< omg <= 2          !
    !                                                                                       !                      
    integer,save::isPrecondition = 0          ! imporove the accuracy and convergence
    !                             0  without preconditioning
    !                             1  with preconditioning                                                                 !
    !---------------------------------------------------------------------------------------!
    !                 Turbulent Model                                                       !
    integer,save:: turModelNum                                                        !            
    !                       0.  doesn't use turbulent model                                 !
    !                       1.  SA                                                          !              
    !                       2.  SST                                                         !  
    character(len=4),parameter::turModelDict(0:2) = ["","-SA ","-SST"]

    ! the scheme to get the properities on the edge used in turbulent model.                !                                                            
    integer,parameter:: schemeType = 2                                                      !           
    !                   1. one order upwind shceme                                          !                           
    !                   2. central difference scheme                                        !                                 
    !---------------------------------------------------------------------------------------!
    !---------------------------------------------------------------------------------------!
    !                   Case Route Setting                                                  !
    ! case route                                                                            !
    character(len=100),save::caseRoute = "D:\Users\myCase\paper"    !  
    !                                                                                       !
    character(len=3),save:: gridNum           

    !determine which file as the word directory                                             !
    character(len=10),save:: flowNum                                                         !

    character(len=30),save:: airfoilName = "naca0012"     ! "OA209-130"                                   

    !determine wheather new gird is used.                                                   !                 
    character(len=1),parameter:: isNewGrid="y"                                              !     
    !                                                                                       !                                                                                       !
    !---------------------------------------------------------------------------------------!   
    character(len=200),save::dirGrid,dirFlow 

contains

subroutine setDir
    !*******************************************************************
    !   Description:  directory setting
    !	Date: 2016-4-28
    !   Modification: 
    !*******************************************************************
    
    dirGrid = trim(adjustL(caseRoute))//"\"//trim(adjustL(gridNum))//"\"
    dirFlow = trim(adjustL(caseRoute))//"\"//trim(adjustL(gridNum))//"\"//trim(adjustL(flowNum))//"\"

end subroutine


end module


!--------------------------------------------------------------------------------------------------------------------------------------------------------
! Modification
!
! 1.               2015-10-27               modify the fifth convetive flux  + Vt*p / conFulx_xx.f90, LU_SGS_xx.f90
! 2.               2015-10-30               add the viscous part in the  rA   / LU_SGS.f90
! 3.               2015-10-30               modify the  wall boundary conditions, u/v/nu/k/omg only have normal gradient , T  only has tangent gradient / visFlux_xxx.f90
! 4.               2015-11-1                the time step calculation: omits the visoucs part when using implicit scheme, which is different from explicit scheme
! 5.               2015-11-1                the viscous strss and energy term  have to consider the part caused by turbulent kinetic energy.
! 6.               2015-11-13               omit the coupled relation between main equations and turbulent equations, that's to say  the K is omitted in E and mu.
! 7.               2015-11-23               precondition: the precondtion matrix before artificial viscosity  is the inverse matrix of the precondition matirx before residual. 
!
!--------------------------------------------------------------------------------------------------------------------------------------------------------
