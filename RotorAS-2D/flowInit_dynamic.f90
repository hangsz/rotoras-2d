subroutine flowInit_dynamic
    !*******************************************************************
    !   Description: initialize for dynamic flow
    !	Date: 2016-4-28
    !   Modification: 
    !*******************************************************************
    implicit none
    
    integer::idRead
    character( len = 200)::filename
    logical::alive
    real(8)::angularFrequency = 0.0d0
    integer::phaseTemp
    
    !----------------------------------------------------
    ! initial data is read from the file of this phase
    phaseTemp = startphase - 1
    
    ! reset process control parameters
    angularFrequency =  2.0d0*kr*Ma_inf*sqrt(gamma*p_inf/rou_inf)
    t_total = 2.0d0*pi/angularFrequency/cycleStep * phaseTemp*steps
    
    write(filename,"(I3)") phaseTemp 
    
    filename = trim(adjustL(dirFlow))//"flow\flow-info-"//trim(adjustL(filename))//".dat"
    
    inquire(file = filename,exist = alive)
    
    if ( .NOT. alive) then
        write(*,*)  filename,'not exist.'
        stop   
    end if
    
    select case( turModelNum )
    case( 0 )
        call flowInit_turNone_dynamic(filename)
    case( 1 )
        call flowInit_SA_dynamic(filename)
    case( 2 )
        call flowInit_SST_dynamic(filename)    
    case default
        
    end select
    
end subroutine
 
    
subroutine flowInit_turNone_dynamic(filename)
    !*******************************************************************
    !   Description: initialize without using turbulent model
    !	Date: 2016-4-28
    !   Modification: 
    !*******************************************************************
    implicit none
    character( len=*),intent(in)::filename
    integer::i
    integer::idRead
    
    open(newunit=idRead,file=filename)
    
        read(idRead,*) 
        read(idRead,*) 
        read(idRead,*) 
        read(idRead,*) 
        read(idRead,*) 
     
        do i=1,nnodes
            read(idRead,*) !xy(1,i)
        end do
        do i=1,nnodes
            read(idRead,*) !xy(2,i)
        end do
    
        do i=1,ncells
            read(idRead,*) U(5,i)
        end do
        do i=1,ncells
            read(idRead,*) U(1,i)
        end do
        do i=1,ncells
            read(idRead,*) U(2,i)
        end do
        do i=1,ncells
            read(idRead,*) U(3,i)
        end do
    
        !do i=1,ncells
        !    read(idRead,*) !icell(:,i)
        !end do
    
    close(idRead)
    
    U(4,:)=0.0d0
   
    W(1,:)=U(1,:)
    W(2,:)=U(1,:)*U(2,:)
    W(3,:)=U(1,:)*U(3,:)
    W(4,:)=0.0d0
    W(5,:)=U(5,:)/(gamma-1.0d0) + U(1,:)*( U(2,:)**2 + U(3,:)**2 )/2.0d0   
    
    muT =0.0d0
    muT_av = 0.0d0

end subroutine
    
    
subroutine flowInit_SA_dynamic(filename)    
    !*******************************************************************
    !   Description: initialize with SA model
    !	Date: 2016-4-28
    !   Modification: 
    !*******************************************************************
  
    implicit none 
    character( len = *),intent(in)::filename
    integer::i
 
    integer::idRead
   
    open(newunit=idRead,file=filename)
    
        read(idRead,*) 
        read(idRead,*) 
        read(idRead,*) 
        read(idRead,*) 
        read(idRead,*) 
     
        do i=1,nnodes
            read(idRead,*) !xy(1,i)
        end do
        do i=1,nnodes
            read(idRead,*) !xy(2,i)
        end do
    
        do i=1,ncells
            read(idRead,*) U(5,i)
        end do
        do i=1,ncells
            read(idRead,*) U(1,i)
        end do
        do i=1,ncells
            read(idRead,*) U(2,i)
        end do
        do i=1,ncells
            read(idRead,*) U(3,i)
        end do
         do i=1,ncells
            read(idRead,*) !Ma
        end do
        
        do i=1,ncells
            read(idRead,*) muT(i)
        end do
        
        do i=1,ncells
            read(idRead,*) U_SA(i)
        end do
        
        !do i=1,ncells
        !    read(idRead,*) !icell(:,i)
        !end do
    
    close(idRead)
    
    U(4,:)=0.0d0
   
    W(1,:)=U(1,:)
    W(2,:)=U(1,:)*U(2,:)
    W(3,:)=U(1,:)*U(3,:)
    W(4,:)=0.0d0
    W(5,:)=U(5,:)/(gamma-1.0d0) + U(1,:)*( U(2,:)**2 + U(3,:)**2 )/2.0d0  
    
    !initilize the turbulent flow
    W_SA = U(1,:) * U_SA
    
   
end subroutine

subroutine flowInit_SST_dynamic(filename)      
    !*******************************************************************
    !   Description: initialize with SST model
    !	Date: 2016-4-28
    !   Modification: 
    !*******************************************************************
    implicit none
    character( len = *),intent(in)::filename
    integer::i
    integer::idRead
    ! purpose: read the stady flow properties
  
    open(newunit=idRead,file=filename)
    
    read(idRead,*) 
    read(idRead,*) 
    read(idRead,*) 
    read(idRead,*) 
    read(idRead,*) 
     
    do i=1,nnodes
        read(idRead,*) !xy(1,i)
    end do
    do i=1,nnodes
        read(idRead,*) !xy(2,i)
    end do
    
    do i=1,ncells
        read(idRead,*) U(5,i)
    end do
    do i=1,ncells
        read(idRead,*) U(1,i)
    end do
    do i=1,ncells
        read(idRead,*) U(2,i)
    end do
    do i=1,ncells
        read(idRead,*) U(3,i)
    end do
    
    do i=1,ncells
        read(idRead,*) !iMa
    end do
    
    do i=1,ncells
        read(idRead,*) muT(i)
    end do
    do i=1,ncells
        read(idRead,*) U_SST(1,i)
    end do
    do i=1,ncells
        read(idRead,*) U_SST(2,i)
    end do
    !do i=1,ncells
    !    read(idRead,*) !icell(:,i)
    !end do
    !
    close(idRead)
    
    U(4,:)=0.0d0
    
    W(1,:)=U(1,:)
    W(2,:)=U(1,:)*U(2,:)
    W(3,:)=U(1,:)*U(3,:)
    W(4,:)=0.0d0
    W(5,:)=U(5,:)/(gamma-1.0d0) + U(1,:)*(U(2,:)**2 + U(3,:)**2)/2.0d0
    
    !initilize the turbulent flow
    W_SST(1,:) = U(1,:) * U_SST(1,:)
    W_SST(2,:) = U(1,:) * U_SST(2,:)
   
    
    
end subroutine
