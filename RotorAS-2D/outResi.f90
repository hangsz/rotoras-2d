subroutine outResi(num,oldRsi,newRsi)
    !*******************************************************************
    !   Description: out residual in static calculation
    !	Date: 2016-4-28
    !   Modification: 
    !*******************************************************************
    implicit none
    integer,intent(in)::num
    real(8),intent(in)::oldRsi(:),newRsi(:)
    integer,save::oriFlag = 1
    real(8),save::oriRsi(5)
    integer,parameter::fileid = 17
    !----------------------------------------------------------
    !write(*,*)   "outResi"
    
    if( oriFlag==1)  then
        oriRsi = oldRsi
        !oriRsi(1) = rou_inf
        !oriRsi(2) = u_inf
        !oriRsi(3) = v_inf
        !oriRsi(5) = p_inf
        oriFlag = 0
    end if
    
    if (num == 1) then
         open(fileid,file=trim(adjustL(dirFlow))//"residual\residual.dat",status = 'REPLACE')
         write(fileid,200)  num,log10(newRsi(1:3)/oriRsi(1:3)),log10(newRsi(5)/oriRsi(5))
         close(fileid)
    else
        if( mod(num,1) == 0  )  then
            open(fileid,file=trim(adjustL(dirFlow))//"residual\residual.dat",position = "APPEND")
            write(fileid,200)  num,log10(newRsi(1:3)/oriRsi(1:3)),log10(newRsi(5)/oriRsi(5))
            close(fileid)
        end if
        
    end if
    
200 format(1X,I5,4(2XF6.3))
    
    
end subroutine
    