subroutine flowInit_static
    !*******************************************************************
    !   Description: initialize for static flow
    !	Date: 2016-4-28
    !   Modification: 
    !*******************************************************************
    implicit none
    
    U(1,:) = rou_inf
    U(2,:) = u_inf
    U(3,:) = v_inf
    U(4,:) = 0.0d0
    U(5,:) = p_inf
    
    W(1,:) = rou_inf
    W(2,:) = rou_inf*u_inf
    W(3,:) = rou_inf*v_inf
    W(4,:) = 0.0d0
    W(5,:) = p_inf/(gamma-1.0d0) + rou_inf*(u_inf**2+v_inf**2)/2.0d0 
   
    !initilize the turbulent flow
    select case( turModelNum )
    case( 0 ) 
        muT = 0.0d0
        muT_av = 0.0d0
    case( 1 )
        U_SA = 3.0d0*muL_inf/rou_inf      ! 3.0-5.0
        W_SA = rou_inf*U_SA

        muT = 3.0d0**2/(3.0d0**2+Cv1_SA**2) * muL_inf
    case( 2 ) 
        U_SST(2,:) = C1_SST*sqrt(u_inf**2 + v_inf**2)/ L_SST
        U_SST(1,:) = muL_inf*10.0d0**(-C2_SST)/rou_inf *U_SST(2,:)
        W_SST(1,:) = U(1,:) * U_SST(1,:)
        W_SST(2,:) = U(1,:) * U_SST(2,:)

        muT = muL_inf*10.0d0**(-C2_SST)
        case default

    end select
    
end subroutine
    
    