 !*******************************************************************
    !	Copyright(c), 2016-2026, Sun Zhenhang
    !   All rights reserved.
    !	
    !	File name: myStatic
    !	Version: 2.0
    !	Author: Sun Zhenhang
    !	Date: 2016-4-28
    !
    !   Description: personal application: which can be used to calculate static properties.
    !	
    !   Modification: 
    !
    !*******************************************************************
    
subroutine myStatic
  
    
    use controlDict,only:Ma_inf,Re_inf,isReverse,reverseDict
    use controlDict,only:conFluxNum,conFluxDict,turModelNum,turModelDict,moveNum,moveDict
    use controlDict,only:gridNum,flowNum
    
    use FVM, only: moveNum, flowNum , att , solver , deallocateMemory 
    
    implicit none
    integer::i
    character::ctrl
    
    !----------------------------------------------------------
    ! setting control parameters
    Ma_inf = 0.6d0
    Re_inf = 4.8E6 
    
    att = 3.16
    
    conFluxNum = 1                                                          !      
    !                   1.  JST scheme                                                      !       
    !                   2.  Roe scheme    
    !--------------------------------------------------        
    turModelNum = 1                                                                    
    !                       0.  doesn't use turbulent model                                 
    !                       1.  SA                                                                        
    !                       2.  SST                                                         
    !---------------------------------   
    moveNum = 0
    !                        0 static
    !                        1 dynamic
    
    isReverse = 1
    
    gridNum = "5"           ! the grid to use 
    flowNum = "3"            ! store in this directory
    
    
    !------------------------------------------------------------------------
    write(*,*)  
    write(*,*)  "!!! Please Check The Control Dictionary Carefully !!!"
    write(*,*)   
    write(*,*)  "-----------------------------------------------------"
    write(*,"(1XA25,3X,A8)")      adjustR("Application Name:"), adjustL("myStatic")
    write(*,"(1XA25,3X,F5.3)")    adjustr("Ma:"), Ma_inf
    write(*,"(1XA25,3X,ES9.3E2)") adjustr("Re:"),Re_inf
    write(*,"(1XA25,3X,F5.3)")    adjustR("att:"), att
    
    write(*,"(1XA25,3X,I1,A8)")   adjustr("moveNum:"), moveNum,moveDict(moveNum)
    write(*,"(1XA25,3X,I1,A4)")   adjustr("isReverse:"), isReverse,reverseDict(isReverse)
    write(*,*)  
    write(*,"(1XA25,3XI1,A4)")    adjustR("conFluxNum:"), conFluxNum, conFluxDict(conFluxNum)
    write(*,"(1XA25,3X,I1,A4)")   adjustR("turModelNum:"), turModelNum, turModelDict(turModelNum)
   
    write(*,"(1XA25,3X,A3)")      adjustr("gridNum:"), gridNum
    write(*,"(1XA25,3X,A8)")      adjustr("flowNum:"),flowNum
    
    write(*,*)  "-----------------------------------------------------"
    
    write(*,*) "Sure? ('y'-continue 'n'-stop)"
    do
        read(*,*) ctrl
        if( 'y' == ctrl) exit 
        if( 'n' == ctrl) stop
        
        write(*,*) "Error, Re-input:"
    end do
            
    !--------------------------------------------------------------------------------
    
    ! solve
    
  
     
    do i= 0,1
       
       call solver
       call deallocateMemory 
       
    end do
    
end subroutine