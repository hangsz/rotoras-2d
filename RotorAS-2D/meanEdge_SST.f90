subroutine meanEdge_SST( U_av,muL_av ,U_Rot)  
    !*******************************************************************
    !   Description:  
    !	Date: 2016-4-28
    !   Modification: 
    !*******************************************************************
 
    implicit none
    integer::i
    integer::ncl,ncr
    real(8),intent(in)::U_av(:,:),muL_av(:),U_Rot(:,:)
    real(8)::ux,uy,Vn
    
    U_av_SST = 0.0d0
    
    do i=1,nedges
        ncl = iedge(3,i)                      ! the number of the left and the right cell
        ncr = iedge(4,i)
        
        ux =  U_av(2,i) - 0.5d0 * ( U_Rot(1,iedge(1,i)) + U_Rot(1,iedge(2,i)) )
        uy =  U_av(3,i) - 0.5d0 * ( U_Rot(2,iedge(1,i)) + U_Rot(2,iedge(2,i)) )
        Vn = ux* vector(1,i)/ds(i)   + uy * vector(2,i)/ds(i)
        !Vn = dot_product( U_av(2:3,i) , vector(:,i) ) 
          
        select case( ncr ) 
        case (-1)                             !-1 represents that the edge is the aerofoil  surface         
            U_av_SST(1,i) = 0.0d0
            U_av_SST(2,i) = 60.0d0*muL_av(i)/( U_av(1,i)*beta1_SST*d(ncl)**2)
        case (-2)                            !-2 represents that the edge is the farfield  boundaries
          
            if ( Vn .LE. 0.0d0) then         !inflow
                U_av_SST(2,i) = C1_SST*ma_inf*sqrt(gamma*p_inf/rou_inf)/L_SST
                U_av_SST(1,i) = muL_inf*10.0**(-C2_SST)/rou_inf * U_av_SST(2,i)
            else
                U_av_SST(:,i) = U_SST(:,ncl)
            end if
            
        case default
            if( schemeType ==1 ) then
                if( Vn > 0.0d0)  then
                    U_av_SST(:,i)= U_SST(:,ncl)
                else
                    U_av_SST(:,i)= U_SST(:,ncr)  
                end if
            else
                U_av_SST(:,i) = 0.5d0*( U_SST(:,ncl) + U_SST(:,ncr) )  
                
            end if
            
        end select
          
    end do
    
end subroutine
