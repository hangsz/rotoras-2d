subroutine allocateMemory_SA
    !*******************************************************************
    !   Description:  
    !	Date: 2016-4-28
    !   Modification: 
    !*******************************************************************

    write(*,*) "allocateMemory_SA"
    
    allocate( U_SA(ncells) )
    
    allocate( W_SA(ncells) )
    !---------------
    ! dual time
   
    allocate( W_SA_n(ncells) )
    allocate( W_SA_n1(ncells) )
    allocate( QW_SA(ncells) )
    !--------------
    allocate( U_av_SA(nedges) )
    
    allocate( Fc_SA(ncells) ) 
    
    allocate( Grad_U_SA(2,ncells) )
    allocate( Grad_W_SA(2,ncells) )
    
    allocate( Fv_SA(ncells) ) 
    allocate( Dissi_SA(ncells) )
    allocate( Q_SA(ncells) )
    allocate( dQdW_SA(ncells) )
    
    allocate( Rsi_SA(ncells) )
    
    
    allocate( S_SA(ncells) )
    allocate( chi_SA(ncells) )
    allocate( fv1_SA(ncells) )
    allocate( fv2_SA(ncells) )
    allocate( fv3_SA(ncells) )
    
    allocate( fw_SA(ncells) )
    allocate( g_SA(ncells) )
    allocate( rT_SA(ncells) )
    
end subroutine
    
subroutine deallocateMemory_SA

    write(*,*) "deallocateMemory_SA"
    
    deallocate( U_SA)
    
    deallocate( W_SA)
    !---------------
    ! dual time
    deallocate( W_SA_n )
    deallocate( W_SA_n1 )
    deallocate( QW_SA )
   
    !--------------
    deallocate( U_av_SA )
    
    deallocate( Fc_SA ) 
    
    deallocate( Grad_U_SA)
    deallocate( Grad_W_SA )
    
    deallocate( Fv_SA ) 
    deallocate( Dissi_SA )
    deallocate( Q_SA)
    deallocate( dQdW_SA )
    
    deallocate( Rsi_SA )
    
    
    deallocate( S_SA )
    deallocate( chi_SA )
    deallocate( fv1_SA )
    deallocate( fv2_SA )
    deallocate( fv3_SA )
    
    deallocate( fw_SA )
    deallocate( g_SA )
    deallocate( rT_SA )
    
end subroutine
