subroutine output(filename)
    !*******************************************************************
    !   Description: out flow properties
    !	Date: 2016-4-28
    !   Modification: 
    !*******************************************************************
    implicit none 
    integer::i
    integer,parameter::fileid=7
    character(len=50)::filename
    !----------------------------------------------------------
    write(*,*)   "Output"
    
    open(fileid,file=trim(adjustL(dirFlow))//"flow/"//trim(adjustL(filename))//".dat")
   
    select case( turModelNum) 
        
    case( 0 )  
        
        write(fileid,*) 'TITLE="Flow properties" '
        write(fileid,*) 'VARIABLES= "X" "Y"  "P" "Rou" "U"  "V" "Ma"' 
        write(fileid,*) "ZONE NODES=",nnodes," ELEMENTS=",ncells," ZONETYPE=FEQUADRILATERAL"
        write(fileid,*) "DATAPACKING=BLOCK"
        write(fileid,*) "VARLOCATION=([3-7]=CELLCENTERED)"
     
        do i=1,nnodes
            write(fileid,*) xy(1,i)
        end do
        do i=1,nnodes
            write(fileid,*) xy(2,i)
        end do
    
        do i=1,ncells
            write(fileid,*) U(5,i)
        end do
        do i=1,ncells
            write(fileid,*) U(1,i)
        end do
        do i=1,ncells
            write(fileid,*) U(2,i)
        end do
        do i=1,ncells
            write(fileid,*) U(3,i)
        end do
        
        do i=1,ncells
            ! ma
            write(fileid,*) sqrt( U(2,i)**2+U(3,i)**2 )/sqrt(gamma*U(5,i)/U(1,i))
        end do
    
        do i=1,ncells
            write(fileid,*) icell(:,i)
        end do
        
    case( 1 )
        
        write(fileid,*) 'TITLE="Flow properties" '
        write(fileid,*) 'VARIABLES= "X" "Y"  "P" "Rou" "U" "V" "Ma" "muT" "nu"' 
        write(fileid,*) "ZONE NODES=",nnodes," ELEMENTS=",ncells," ZONETYPE=FEQUADRILATERAL"
        write(fileid,*) "DATAPACKING=BLOCK"
        write(fileid,*) "VARLOCATION=([3-9]=CELLCENTERED)"
     
        do i=1,nnodes
            write(fileid,*) xy(1,i)
        end do
        do i=1,nnodes
            write(fileid,*) xy(2,i)
        end do
    
        do i=1,ncells
            write(fileid,*) U(5,i)
        end do
        do i=1,ncells
            write(fileid,*) U(1,i)
        end do
        do i=1,ncells
            write(fileid,*) U(2,i)
        end do
        do i=1,ncells
            write(fileid,*) U(3,i)
        end do
        
        do i=1,ncells
            ! ma
            write(fileid,*) sqrt( U(2,i)**2+U(3,i)**2 )/sqrt(gamma*U(5,i)/U(1,i))
        end do
    
        do i=1,ncells
            write(fileid,*)  muT(i)
        end do
        
        do i=1,ncells
            write(fileid,*)  U_SA(i)
        end do
        do i=1,ncells
            write(fileid,*) icell(:,i)
        end do
        
    case( 2 )
        
        write(fileid,*) 'TITLE="Flow properties" '
        write(fileid,*) 'VARIABLES= "X" "Y"  "P" "Rou" "U"  "V" "Ma" "mu" "k" "omg" ' 
        write(fileid,*) "ZONE NODES=",nnodes," ELEMENTS=",ncells," ZONETYPE=FEQUADRILATERAL"
        write(fileid,*) "DATAPACKING=BLOCK"
        write(fileid,*) "VARLOCATION=([3-10]=CELLCENTERED)"
     
        do i=1,nnodes
            write(fileid,*) xy(1,i)
        end do
        do i=1,nnodes
            write(fileid,*) xy(2,i)
        end do
    
        do i=1,ncells
            write(fileid,*) U(5,i)
        end do
        do i=1,ncells
            write(fileid,*) U(1,i)
        end do
        do i=1,ncells
            write(fileid,*) U(2,i)
        end do
        do i=1,ncells
            write(fileid,*) U(3,i)
        end do
        
        do i=1,ncells
            ! ma
            write(fileid,*) sqrt( U(2,i)**2+U(3,i)**2 )/sqrt(gamma*U(5,i)/U(1,i))
        end do
    
        do i=1,ncells
            write(fileid,*)  muT(i)
        end do
        do i=1,ncells
            write(fileid,*)  U_SST(1,i)
        end do
        
        do i=1,ncells
            write(fileid,*)  U_SST(2,i)
        end do
        
        do i=1,ncells
            write(fileid,*) icell(:,i)
        end do
        
    case default
    
    end select
    
    
    close(fileid)
  
end subroutine 
