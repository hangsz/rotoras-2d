 !*******************************************************************
    !	Copyright(c), 2016-2026, Sun Zhenhang
    !   All rights reserved.
    !	
    !	File name: myDynamic
    !	Version: 2.0
    !	Author: Sun Zhenhang
    !	Date: 2016-4-28
    !
    !   Description: personal application: which can be used to calculate dynamic properties.
    !	
    !   Modification: 
    !
    !*******************************************************************
    
    
subroutine myDynamic
    ! calcualte static and dynamic state sequentially 

    use controlDict,only:rot_cen  !!!!!!!!!!!!!!!!!
    
    use controlDict,only:Ma_inf,Re_inf,att,att_ampl,kr,isReverse,reverseDict,startPhase
    use controlDict,only:conFluxNum,conFluxDict,turModelNum,turModelDict,moveNum,moveDict
    use controlDict,only:gridNum,flowNum
    use FVM, only: solver, deallocateMemory  

    
    character::ctrl
    
    !*********************************************************************************
    ! setting the parameters
    Ma_inf = 0.36d0
    Re_inf = 13.0E6 * Ma_inf 

    att = 2.89d0            ! attack of angle,��
    att_ampl = 2.41d0       ! rotational amplitude,��           
    kr = 0.0808d0           ! reduced frequency    

    ! moveNum = 0
    !                        0 static
    !                        1 dynamic
    isReverse = 0           

    startPhase = 1           ! if diverge, restart from the step that diverge

    conFluxNum = 1                                                              
    !                   1.  JST scheme                                            
    !                   2.  Roe scheme    
    !--------------------------------------------------        
    turModelNum = 1                                                        
    !                       0.  doesn't use turbulent model                                
    !                       1.  SA                                                                     
    !                       2.  SST                                                          
    !--------------------------------- -----------------  

    gridNum = "4"           ! the grid to use 
    flowNum = "14"            ! store in this directory
    !******************************************************************


    !------------------------------------------------------------------
    write(*,*)  
    write(*,*)  "!!! Please Check The Control Dictionary Carefully !!!"
    write(*,*)   
    write(*,*)  "-----------------------------------------------------"
    write(*,"(1XA25,3X,A9)")         adjustR("Application Name:"), "myDynamic"
    write(*,"(1XA25,3X,F5.3)")       adjustR("Ma:"), Ma_inf
    write(*,"(1XA25,3X,ES9.3E2)")    adjustR("Re:"),Re_inf
    write(*,"(1XA25,3X,F6.3)")       adjustR("att:"), att
    write(*,"(1XA25,3X,F6.3)")       adjustR("att_ampl:"), att_ampl
    write(*,"(1XA25,3X,F6.4)")       adjustR("kr:"), kr
    write(*,"(1XA25,3XF6.4,3XF6.4)") adjustR("rot_cen:"), rot_cen(1),rot_cen(2) 
    write(*,*)  
    write(*,"(1XA25,3X,I3)")         adjustR("startPhase:"),startPhase
    write(*,"(1XA25,3X,A29)")        adjustR("moveNum:"),adjustL("Auto Set Depend on startPhase")
    write(*,"(1XA25,3X,I1,A4)")      adjustR("isReverse:"), isReverse,reverseDict(isReverse)
    write(*,*)  
    write(*,"(1XA25,3XI1,A4)")       adjustR("conFluxNum:"), conFluxNum, conFluxDict(conFluxNum)
    write(*,"(1XA25,3X,I1,A4)")      adjustR("turModelNum:"), turModelNum, turModelDict(turModelNum)

    write(*,"(1XA25,3X,A3)")         adjustR("gridNum:"), gridNum
    write(*,"(1XA25,3X,A10)")        adjustR("flowNum:"), adjustL(flowNum)

    write(*,*)  "-----------------------------------------------------"
  
    
    write(*,*) "Sure? ('y'-continue 'n'-stop)"
    do
        read(*,*) ctrl
        if( 'y' == ctrl) exit 
        if( 'n' == ctrl) stop
        
        write(*,*) "Error, Re-input:"
    end do
    !------------------------------------------------------------------

    ! solve 
    ! calculate the static flow at the mean angle(att)
    if( startPhase == 1 ) then
        moveNum = 0    
        call solver
        call deallocateMemory

    end if

    ! calculate the dynamic flow 
    moveNum = 1
    call solver
    call deallocateMemory


end subroutine