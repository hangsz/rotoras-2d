subroutine solver
    !*******************************************************************
    !   Description:  main function and start funcion 
    !	Date: 2016-4-28
    !   Modification: 
    !*******************************************************************
   
    implicit none
    !----------------------------------------------------
    write(*,*)  "solver"
    !----------------------------------------------------------------
    
    ! initialize
    call setDir                        ! set the directionary
                   
    call Grid                          ! prepare the grid
    
    call allocateMemory              
    call Reorder                       ! reordering is needed for LU-SGS
    
    select case( turModelNum ) 
    case( 1 )
        call allocateMemory_SA
    case( 2 )
        call allocateMemory_SST
    case default 
    end select

    call flowInit
    call outputFreeFlow
    
    
    if( moveNum /= 0 )  then
        call motionInit
        dt_r = 2.0d0*pi/angularFrequency/cycleStep
        endPhase = cycleNum*cycleStep/steps
    else
        endPhase = staticMax/steps
    end if

    if( 1 == isPrecondition  )  then
        call allocateMemory_Pre
    end if

    call dualTime

end subroutine 

subroutine dualTime
    !*******************************************************************
    !   Description: dual time method
    !	Date: 2016-4-28
    !   Modification: 
    !*******************************************************************
    implicit none
    integer::i,j,k
    integer::iter                    !iterative variable
    integer::count 
    integer::flag                    !the variable to judge wheather converges
    character(len=50)::filename
    real(8)::AoA                     ! attack angel
    real(8)::oldRsi(5),newRsi(5)
    real(8)::RsiTemp(5)

               
    ! initialize Wn and Wn1
    Wn = W                                          ! the n step conservative variables of main control equations
    Wn1 = W                                         ! the n+1 step conservative variables of main control equations

    !------------------------------
    select case( turModelNum )      
    case( 0 )        
    case( 1 ) 
        W_SA_n = W_SA                            ! the n step conservative variables of turbulent control equations       
        W_SA_n1 = W_SA   
    case( 2 )
        W_SST_n = W_SST                          ! the n step conservative variables of turbulent control equations       
        W_SST_n1 = W_SST  
        case default 
    end select        

    !-----------------------------
    do i=startPhase,endPhase 

        do iter = 1,steps

            if( moveNum /=0 ) then 
                t_total = t_total + dt_r
                call rotation( t_total,U_Rot,AOA )
            else
                U_rot = 0.0
                AoA = att
            end if

            !-------------------------------------
            Wn1 = Wn
            Wn = W
            select case( turModelNum ) 
            case( 0 ) 
            case( 1 ) 
                W_SA_n1 = W_SA_n                            ! the n step conservative variables of turbulent control equations       
                W_SA_n = W_SA   
            case( 2 )
                W_SST_n1 = W_SST_n                          ! the n step conservative variables of turbulent control equations       
                W_SST_n = W_SST  
                case default 
            end select  

            !------------------------------------
            do j =1,5
                QW(j,:) = 2.0d0/dt_r *vol*Wn(j,:) -0.5d0/dt_r * vol*Wn1(j,:)
            end do

            select case( turModelNum ) 
            case( 0 ) 
            case( 1 ) 
                QW_SA = 2.0d0 /dt_r *vol*W_SA_n -0.5d0/dt_r * vol*W_SA_n1
            case( 2 )
                do j=1,2
                    QW_SST(j,:) = 2.0d0 /dt_r *vol*W_SST_n(j,:) -0.5d0/dt_r * vol*W_SST_n1(j,:)
                end do  
            case default 
            end select  

            do count = 1,inStep 
                
                write(*,*)  count , iter, i
                write(*,*) "t:",t_total,"dt_r:",dt_r
                if(moveNum /=0 ) then
                    write(*,"(1XA5F6.2,3XA4F5.3,3XA4ES9.3E2,3XA4F6.4)") "AoA: ",AoA,"Ma: ",Ma_inf,"Re: ",Re_inf,"kr: ",kr
                else
                    write(*,"(1XA5F6.2,3XA4F5.3,3XA4ES9.3E2)") "AoA: ",AoA,"Ma: ",Ma_inf,"Re: ",Re_inf
                end if

                if( isPrecondition== 0) then
                    call meanEdge            ! get the primative variables on every edge
                else
                    call meanEdge_pre        ! get the primative variables on every edge 
                end if

                call gradient                ! get the gradient of every cell using the values calculated from meanEdge

                !---------------------------------------------------
                ! get the turbulent properies on the edge
                select case( turModelNum ) 
                case( 1 )
                    call getMuT_SA( U_av,muT,muT_av,U_Rot)
                case( 2 )
                    call getMUT_SST(U_av,muT,muT_av,U_Rot)
                    case default 
                end select

                !-------------------------------------------
                ! convective flux
                select case( conFluxNum )
                case( 1 )
                    call conFlux_JST
                    if( isPrecondition == 0 ) then
                        call artDissipation
                        dt = CFL*vol/lamda_c 
                        !write(*,*)  "dtMax:",maxval(dt) 
                    else
                        call withPre(U,U_av,U_Rot,muL_av,muT_av,dt )
                        call artDissipation( Palf_c,PinvM )
                    end if

                case( 2 ) 
                    !get the convective flux using Roe scheme 
                    Dissi = 0.0 
                    call conFlux
                    dt = CFL*vol/lamda_c 

                    !if( isPrecondition== 0 ) then
                    !    call conFlux
                    !    dt = CFL*vol/lamda_c 
                    !else
                    !   call withPre(U,U_av,U_Rot,muL_av,muT_av,dt )
                    !   call conFlux

                end select

                ! get viscous flux
                call visFlux        
                !--------------------------

                Rsi = Fc - Dissi - Fv

                do j=1,5
                    Rsi(j,:) = Rsi(j,:) + 1.5d0/dt_r *vol*W(j,:) - QW(j,:)   
                end do

                !---------------------------------
                ! precondition
                if( isPrecondition==1) then
                    do k=1,ncells
                        RsiTemp = Rsi(:,k)
                        do j=1,5
                            Rsi(j,k) = dot_product( MinvSP(j,:,k),RsiTemp )
                        end do
                    end do
                end if

                !---------------------------------------
                ! LU-SGS
                select case( turModelNum ) 
                case( 0 ) 
                    call LU_SGS   
                case( 1 )
                    call Resi_SA( U,U_av,alf_c,muL,muL_av,Grad,U_Rot )                ! get  muT
                    Rsi_SA = Rsi_SA + 1.5d0/dt_r *vol*W_SA - QW_SA               
                    call LU_SGS_SA
                case( 2 )
                    call Resi_SST(U,U_av,alf_c,muL,muL_av,muT,muT_av,Grad, U_Rot )     ! get  muT
                    do j=1,2
                        Rsi_SST(j,:) = Rsi_SST(j,:) + 1.5d0/dt_r *vol*W_SST(j,:) - QW_SST(j,:)
                    end do    
                    call LU_SGS_SST  
                case default       
                end select  

                !-----------------------------  
                ! judge wheather this calculation has diverged
                currentPhase = i 
                call diverge
                if( isDiverge )  stop
                !----------------------------------------------------

                ! using the third step residual as the initial residual
                if( count<=3 )  oldRsi = sum( abs(Rsi),2)/ncells
                newRsi = sum( abs(Rsi),2 )/ncells

                call converge( flag,oldRsi,newRsi)

                if( 1==flag .OR. 2==flag )  then
                    write(*,*)  "...Inner Converge..."
                    exit 
                end if 

            end do
            !-------------------------------------------------
            ! output residual
            if( 0==moveNum )  then
                call outResi( (i-1)*steps+iter,oldRsi,newRsi ) 
                if( 2==flag)  then
                    write(*,*)  "...Converge..."
                    return
                end if
            end if
            !-----   
        end do

        if( 0==moveNum) then
            filename = "flow-info-0"
          !  write(filename,"(I3)") i    
          ! filename = "flow-info-"//trim(adjustL(filename))
        else
            write(filename,"(I3)") i    
            filename = "flow-info-"//trim(adjustL(filename))
        end if

        call output(filename)    
    end do

end subroutine 




