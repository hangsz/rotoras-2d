subroUTine conFlux_SST( U_av,U_Rot)     
    !*******************************************************************
    !   Description:  
    !	Date: 2016-4-28
    !   Modification: 
    !*******************************************************************
    implicit none 
    real(8),intent(in)::U_av(:,:),U_Rot(:,:)     
    
    integer::i
    integer::ncl,ncr                        
                                      
    real(8)::ux,uy,Vn                                     

   
    Fc_SST = 0.0d0
    do i = 1,nedges
        ncl=iedge(3,i)
        ncr=iedge(4,i)
        
        ux =  U_av(2,i) - 0.5d0 * ( U_Rot(1,iedge(1,i)) + U_Rot(1,iedge(2,i)) )
        uy =  U_av(3,i) - 0.5d0 * ( U_Rot(2,iedge(1,i)) + U_Rot(2,iedge(2,i)) )
        Vn = ux* vector(1,i)   + uy * vector(2,i)
       
        
        Fc_SST(:,ncl)  = Fc_SST(:,ncl) + U_av(1,i)*U_av_SST(:,i) * Vn
       
        if ( ncr .GT. 0) then
            Fc_SST(:,ncr)  = Fc_SST(:,ncr) - U_av(1,i)*U_av_SST(:,i) * Vn
        end if

    end do
    
end subroUTine
