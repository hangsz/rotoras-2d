    !*******************************************************************
    !	Copyright(c), 2016-2026, Sun Zhenhang
    !   All rights reserved.
    !	
    !	File name: 
    !	Version: 2.0
    !	Author:
    !	Date: 2016-4-28
    !
    !   Description: SA model 
    !	
    !*******************************************************************

module SA
    use controlDict
    use gridInfo
    
    implicit none
    
    real(8),allocatable,save:: U_SA(:),U_av_SA(:)
    real(8),allocatable,save:: W_SA(:)
    
    real(8),allocatable,save:: Q_SA(:),dQdW_SA(:)
    
    real(8),allocatable,save:: Grad_U_SA(:,:),Grad_W_SA(:,:),Fc_SA(:),Fv_SA(:),Dissi_SA(:),Rsi_SA(:)
    
    real(8),allocatable,save:: S_SA(:)
    real(8),allocatable,save:: chi_SA(:),fv1_SA(:),fv2_SA(:),fv3_SA(:)
    real(8),allocatable,save:: fw_SA(:),g_SA(:),rT_SA(:)
    
    !real(8),parameter::Cprod_SA = 2.0d0
    real(8),parameter::Cb1_SA = 0.1355d0,Cb2_SA = 0.622d0 , &
             Cv1_SA = 7.1d0, Cv2_SA = 5.0d0, sigma_SA =2.0d0/3.0d0, kapa_SA = 0.41d0 , &
             Cw2_SA =0.3d0 , Cw3_SA = 2.0d0 
             !Ct1 =1.0 ,Ct2 = 2.0,Ct3 = 1.3, Ct4 = 0.5   tansition
             
    real(8),parameter::Cw1_SA=Cb1_SA/kapa_SA**2+(1.0d0+Cb2_SA)/sigma_SA   ! 3.23906782  
     
    ! dual time
    real(8),allocatable,save:: W_SA_n(:),W_SA_n1(:),QW_SA(:)
    
contains
    
   ! turbulent model--SA
   include "allocateMemory_SA.f90"
   include "meanEdge_SA.f90"
   include "conFlux_SA.f90"
   include "artDissipation_SA.f90"
   include "gradient_SA.f90"
   include "visFlux_SA.f90"
   include "Resi_SA.f90"
   include "getMut_SA.f90"
             
end module


    