  subroutine gradient
    !*******************************************************************
    !   Description:  calculate the gradient in cell center.
    !	Date: 2016-4-28
    !   Modification: 
    !*******************************************************************
    implicit none
    integer::i,j
    integer::ncl,ncr
    
    !allocate memory
    
    Grad = 0.0d0
    
    !the first iteration along_SA the edg_SAe,calculate the gradient of u,v,T
    do i=1,nedges
        
        ncl=iedge(3,i)                      !atain the number of the left and the rig_SAht cell
        ncr=iedge(4,i)
      
        if ( ncr > 0 ) then 
            do j=1,6  
              !the gradient of u,v,T in every cell,left plus and rig_SAht subtract
              Grad(:,j,ncl) = Grad(:,j,ncl) + U_av(j,i)*vector(:,i)
              Grad(:,j,ncr) = Grad(:,j,ncr) - U_av(j,i)*vector(:,i)
            end do    
        else
            do j=1,6  
                Grad(:,j,ncl) = Grad(:,j,ncl) + U_av(j,i)*vector(:,i) 
            end do 
        end if 
    end do
    
    do j=1,6
       Grad(1,j,:) = Grad(1,j,:)/vol
       Grad(2,j,:) = Grad(2,j,:)/vol
    end do
    
 end subroutine  
  
