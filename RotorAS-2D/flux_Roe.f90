subroutine flux_Roe(UL,UR,edgeNum,flux)   
    !*******************************************************************
    !   Description:  calculate convective flux.
    !	Date: 2016-4-28
    !   Modification: 
    !*******************************************************************
   
    implicit none
   
    real(8),intent(in)::UL(:),UR(:)                     ! rou, u, v, w, p of the left and right state of each edge
    real(8),intent(out)::flux(5)
    integer,intent(in)::edgeNum
  
    real(8)::ux,uy,Vn,ut,vt,Vnt
    real(8)::nx,ny
   
    real(8)::HL,HR,VnL,VnR
    real(8)::rou_Roe,u_Roe,v_Roe,H_Roe,q2_Roe,c_Roe,Vn_Roe
    real(8)::FcL(5),FcR(5),dF1(5),dF234(5),dF5(5)
    
    real(8)::delta                               ! Harten's entropy correction
    real(8)::lamda_c1,lamda_c234,lamda_c5        ! |V_Roe - c_Roe|,|V_Roe|,|V_Roe + c_Roe|

    real(8)::coef1,coef21,coef22,coef5
    !--------------------------------------------------------
    ! write(*,*)  "flux_Roe"
  
    nx=vector(1,edgeNum)/ds(edgeNum)
    ny=vector(2,edgeNum)/ds(edgeNum) 
    
    ut =  0.5d0 * ( U_Rot(1,iedge(1,edgeNum)) + U_Rot(1,iedge(2,edgeNum)) )
    vt =  0.5d0 * ( U_Rot(2,iedge(1,edgeNum)) + U_Rot(2,iedge(2,edgeNum)) )
    Vnt = ut* nx   + vt * ny
       
    HL= UL(5)/UL(1)*gamma / (gamma-1.0d0) + ( UL(2)**2 + UL(3)**2 ) /2.0d0 
    HR= UR(5)/UR(1)*gamma / (gamma-1.0d0) + ( UR(2)**2 + UR(3)**2 ) /2.0d0 
    
    !seven Roe's averaging flow properities 
    rou_Roe= sqrt( UL(1)*UR(1) )
    u_Roe = ( UL(2)*sqrt(UL(1)) + UR(2)* sqrt(UR(1)) ) / ( sqrt(UL(1)) + sqrt(UR(1)) )
    v_Roe = ( UL(3)*sqrt(UL(1)) + UR(3)* sqrt(UR(1)) ) / ( sqrt(UL(1)) + sqrt(UR(1)) )
    H_Roe=  ( HL* sqrt(UL(1))  + HR*sqrt(UR(1)) ) / ( sqrt(UL(1)) + sqrt(UR(1)) )
    
    q2_Roe = u_Roe**2 + v_Roe**2
    c_Roe = sqrt( (gamma-1.0d0)*(H_Roe-q2_Roe/2.0d0) )
            
    Vn_Roe = u_Roe * nx + v_Roe * ny 
    !------------------------------------------------
    !the normal speed of each edge has to subtract the grid moving speed 
    VnL = UL(2)*nx + UL(3)*ny - Vnt
    VnR = UR(2)*nx + UR(3)*ny - Vnt
    !-----------------------------------------
                
    FcL(1) = UL(1)*VnL
    FCL(2) = UL(1)*UL(2)*VnL + nx*UL(5)
    FcL(3) = UL(1)*UL(3)*VnL + ny*UL(5)
    FcL(4) = 0.0d0
    FcL(5) = UL(1)*HL*VnL
    FcL(5) = FcL(5) + Vnt*UL(5)
        
    FcR(1) = UR(1)*VnR   
    FCR(2) = UR(1)*UR(2)*VnR + nx*UR(5)
    FcR(3) = UR(1)*UR(3)*VnR + ny*UR(5)
    FcR(4) = 0.0d0
    FcR(5) = UR(1)*HR*VnR
    FcR(5) = FcR(5) + Vnt*UR(5)
    !-------------------------------------------------------
    ! Harten's entropy correction                    
            
    lamda_c1 = abs(Vn_Roe - c_Roe -  Vnt)
    lamda_c234 = abs(Vn_Roe -  Vnt)
    lamda_c5 = abs(Vn_Roe + c_Roe -  Vnt)
    
    !delta = 0.1d0*c_Roe                          ! 0.1 times of the local sound speed   
    delta = 0.1d0*lamda_c5                        ! 0.1 times of the local sound speed   
    
    if( lamda_c1 <= delta )   lamda_c1 = (lamda_c1**2 + delta**2) / (2*delta)
    if( lamda_c234 <= delta ) lamda_c234 = (lamda_c234**2 + delta**2) / (2*delta)
    if( lamda_c5 <= delta )   lamda_c5 = (lamda_c5**2 + delta**2) / (2*delta)
            
    !------------------------------------------------------------
    
    coef1 =  lamda_c1*( UR(5)-UL(5) - rou_Roe*c_Roe*(VnR-VnL) ) / ( 2.0d0*c_Roe**2 )
         
    dF1(1) = coef1
    dF1(2) = coef1 * (u_Roe - c_Roe*nx)
    dF1(3) = coef1 * (v_Roe - c_Roe*ny)
    dF1(4) = 0.0d0
    dF1(5) = coef1 * (H_Roe - c_Roe*Vn_Roe)
    
    coef21 =  lamda_c234 * ( UR(1)-UL(1) - ( UR(5)-UL(5) ) / c_Roe**2 )
    coef22 =  lamda_c234 * rou_Roe
    
    dF234(1) = coef21
    dF234(2) = coef21 * u_Roe + coef22 * ( UR(2)-UL(2) -(VnR-VnL)*ny ) 
    dF234(3) = coef21 * v_Roe + coef22 * ( UR(3)-UL(3) -(VnR-VnL)*ny ) 
    dF234(4) = 0.0d0
    dF234(5) = coef21 * q2_Roe/2.0d0 + coef22 * ( u_Roe*(UR(2)-UL(2)) + v_Roe*(UR(3)-UL(3)) - Vn_Roe*(VnR-VnL) )
     
    coef5 = lamda_c5*( UR(5)-UL(5) + rou_Roe*c_Roe*(VnR-VnL) ) / ( 2.0d0*c_Roe**2 ) 
    dF5(1) = coef5
    dF5(2) = coef5 * (u_Roe + c_Roe*nx)
    dF5(3) = coef5 * (v_Roe + c_Roe*ny)
    dF5(4) = 0.0d0
    dF5(5) = coef5 * (H_Roe + c_Roe*Vn_Roe)
        
    
    ! the convective flux, ledt plus and right subtract
    flux =  0.5d0* ( FcL + FcR - dF1-dF234-dF5 )
       
end subroutine


    


 
