    module  controlPara    !Control parameters module
    implicit none

    !-----------------------------------------------------------------------------
    !constant parameters
    real(8),parameter::pi=3.1415926535898                
    !------------------------------------------------------------------------------

    !incoming flow information
    real(8),save::Ma_inf= 0.6d0                !the mach number
    real(8),save::Re_inf = 4.8E6               ! the Re number

    real(8),save::att=3.160d0                     !attack angle,��

    real(8),save::rou_inf=1.225d0               !density,kg/m^3 
    real(8),save::p_inf=1.01325E5               !pressure,N/m^2
    real(8),save::muL_inf=1.78E-5               !molecular viscocity, pa

    integer,parameter::inflowNum = 1
    ! 1. given  Ma Re rou_inf p_inf, const c=1 , calculate muL_inf
    ! 2. given  Ma rou_inf p_inf, muL_inf, const c=1, no need of Re  
    integer,parameter::isReverse = 0                
    ! 0. 
    ! 1. reverse flow

    real(8),parameter::C=110.6d0                  !sutherland formula constant
    real(8),parameter::R=287.0d0                  !the ideal gas constant,J/(kg*K)      
    real(8),parameter::gamma=1.4d0                !the specific heat ratio
    real(8),parameter::PrL=0.72d0                 !Prantle coefficient
    real(8),parameter::PrT=0.9d0                  !Prantle coefficient
    !------------------------------
    !time step
    real(8),parameter::C_time = 4.0d0
    !
    !--------------------------------------------------------------------------------------
    !---------------------------------------------------------------------------------------!
    !                             Finite volume method                                      !                            
    !                                                                                       !
    !artifical dissipation,k2(1/2,1),k4(1/256,1/32)                                         !
    real(8),parameter::k2=0.6d0,k4=1.0d0/64.0d0                                             !
    !                                                                                       !
    real(8),save::eps=1.0E-8           !the convergence precision                           !
    real(8),save::eps_in=1.0E-3        !inner iteration,the convergence precision           !
    !                                                                                       !
    real(8),save::CFL=1.0E4          !CFL number,implicit, 10E4~10E6                      !

    integer,save::isPrecondition = 0  
    !                             0  without preconditioning
    !                             1  with preconditioning
    !-------------------                                                                    !
    !LU-SGS,is used to calculate the D matrix                                               !
    real(8),parameter::omg = 1.5d0            !overrelaxation factor , 1< omg <= 2          !
    !                                                                                       !                      
    !dual time                                                                              !
    integer,save::inStep = 300           ! the dual time scheme iterative times             !
    !---------------------------------------------------------------------------------------!
    !---------------------------------------------------------------------------------------!
    !                         static  or dynamic                                            !
    integer,save::moveNum = 1    
    !
    integer,save::startPhase = 1
    !                                                                    !
    !                    integer.  the state which to start from (read the preceding state) !
    !                                                                                       !
    integer,save::staticMax= 100           ! static                                         ! 
    ! dynamic                                                                                      !
    integer,save::cycleNum = 3              ! three cycles                                  !       
    integer,save::cycleStep= 300            ! steps every  period                           !
    integer,save::steps = 5                 ! if reach the steps, output                    ! 
    !                                                                                       !
    !integer,parameter::phase= 24                                                           !
    real(8),save::dt_r = 0.001d0                    ! real time step                        !
    !
    real(8),parameter::rot_cen(2)=[0.25d0,0.0d0]   !rotational center,(m,m)                 !
    real(8),save::att_ampl = 4.59d0                 !rotational amplitude,��                 !
    real(8),save::kr = 0.0811d0                      !reduced frequency                      !
    !---------------------------------------------------------------------------------------!
    !---------------------------------------------------------------------------------------!
    !              onvective flux scheme                                                    !
    integer,save::conFluxNum = 1                                                          !      
    !                   1.  JST scheme                                                      !       
    !                   2.  Roe scheme                                                      !
    !                                                                                       !
    ! Roe scheme                                                                            !
    ! limiter                                                                               !
    real(8),parameter::K = 5.0d0                                                            !
    !---------------------------------------------------------------------------------------!
    !---------------------------------------------------------------------------------------!
    !                 turbulent model                                                       !
    integer,save:: turModelNum = 1                                                          !            
    !                       0.  doesn't use turbulent model                                 !
    !                       1.  SA                                                          !              
    !                       2.  SST                                                         !  
    !---------------------------------                                                      !
    ! the scheme to calculate the properities on the edge                                   !
    ! used in turbulent model.                                                              !
    integer,parameter:: schemeType = 2                                                      !           
    !                   1. one order upwind shceme                                          !                           
    !                   2. central difference scheme                                        !                                 
    !---------------------------------------------------------------------------------------!
    !---------------------------------------------------------------------------------------!
    !                     set the case route and grid number                                !
    ! case route                                                                            !
    character(len=100),save::caseRoute = "D:\Users\Hang\Documents\Visual Studio\Case-re"    !  
    !                                                                                       !
    character(len=3),save:: gridNum = "10"             ! 1. triangle grid   

    !determine which file as the word directory                                             !
    character(len=3),save:: flowNum = "1"                                                   !

    character(len=30),save:: airfoilName = "naca0012"
    !character(len=30),save:: airfoilName= "OA209-130"!
    !                                                      ! 2. hybird grid                 !
    !determine wheather new gird is used.                                                   !                 
    character(len=1),parameter:: isNewGrid="y"                                              !     
    !                                                                                       !                                                                                       !
    !---------------------------------------------------------------------------------------!   

    character(len=200),save::dirGrid,dirFlow 

    contains

    subroutine setDir

    dirGrid = trim(adjustL(caseRoute))//"/"//trim(adjustL(gridNum))//"/"
    dirFlow = trim(adjustL(caseRoute))//"/"//trim(adjustL(gridNum))//"/"//trim(adjustL(flowNum))//"/"

    end subroutine


    end module



    ! modify
    !------------------------------------------------------------------------------
    ! 1.               10-27-15               modify the fifth convetive flux  + Vt*p / conFulx_xx.f90, LU_SGS_xx.f90
    ! 2.               10-30-15               add the viscous part in the  rA   / LU_SGS.f90
    ! 3.               10-30-15               modify the  wall boundary conditions, u/v/nu/k/omg only have normal gradient , T  only has tangent gradient / visFlux_xxx.f90
    ! 4.               11- 1-15               the time step calculation: omits the visoucs part when using implicit scheme, which is different from explicit scheme
    ! 5.               11- 1-15               the viscous strss and energy term  have to consider the part caused by turbulent kinetic energy.
    ! 6.               11-13-15               omit the coupled relation between main equations and turbulent equations, that's to say  the K is omitted in E and mu.
    ! 7.               11-23-15               precondition: the precondtion matrix before artificial viscosity  is the inverse matrix of the preconditon matirx before residual. 
    !
    !------------------------------------------------------------------------