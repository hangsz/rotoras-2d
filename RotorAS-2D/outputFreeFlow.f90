subroutine outputFreeFlow
    !*******************************************************************
    !   Description: out inflow properties
    !	Date: 2016-4-28
    !   Modification: 
    !*******************************************************************
    implicit none
    integer::phase
    real(8),allocatable::AOA(:)
    integer::idFree
    integer::i
    !------------------------------------
    write(*,*)  "outputFreeFlow"
    
    
    if(moveNum==0) then
        phase = 0 
    else
        phase = cycleNum*cycleStep/steps   
    end if
    
    allocate( AOA(0:phase) )
    
    do i=0, phase
        AoA(i) = att + att_ampl*sin(2.0d0*pi/cycleStep*steps*i)
    end do
    
    open( newunit=idFree,file=trim(adjustL(dirFlow))//"freeFlow/freeFlowDict.dat")
        write(idFree,*)   att
        write(idFree,*)   phase
        
        do i=0,phase
            write(idFree,*)   i, AOA(i)
        end do
        
       ! write(idFree,*)   AOA
        write(idFree,*)  " ma  rou  p "
        write(idFree,*)   Ma_inf, rou_inf, P_inf
        
    close(idFree)
     
    deallocate( AOA )
    
end subroutine 