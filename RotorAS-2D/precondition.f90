    !*******************************************************************
    !	Copyright(c), 2016-2026, Sun Zhenhang
    !   All rights reserved.
    !	
    !	File name: precondition
    !	Version: 2.0
    !	Author: Sun Zhenhang
    !	Date: 2016-4-28
    !
    !   Description: with precondition, the program can be used to calculate low-mach flow
    !	
    !*******************************************************************

module precondition
    use controlDict
    use gridInfo
    
    implicit none
    real(8),allocatable,save::Palf_c(:),Palf_v(:)
    real(8),allocatable,save::Plamda_c(:),Plamda_v(:)
    real(8),allocatable,save::SPalf_c(:),SPalf_v(:)
    real(8),allocatable,save::SPlamda_c(:),SPlamda_v(:)
    real(8),allocatable,save::PinvM(:,:,:),MinvSP(:,:,:)
    
    real(8),parameter::kpre = 3.0d0
    !-------------------------------------------------------
    !              Variables Specification
    ! Palf_c, Palf_v          :    preconditioned spectral radius of edge, used to calculate the artificial dissipation in the JST scheme
	! Plamada_c, Plamda_v     :    preconditioned spectral radius of cell, used to calculate the artificial dissipation in the JST scheme
    !--------------------------------------------------------
    
    
    
contains
    
subroutine withPre(U,U_av,U_Rot,muL_av,muT_av,dt )
    !*******************************************************************
    !   Description: 
    !	Date: 2016-4-28
    !   Modification: 
    !*******************************************************************
    implicit none
   
    real(8),intent(in)::U(:,:),U_av(:,:),U_Rot(:,:),muL_av(:),muT_av(:)
    real(8),intent(out)::dt(:)
   
    call spectral_P( U_av,U_Rot,muL_av,muT_av ,dt)
    call spectral_SP(U_av,U_Rot,muL_av,muT_av, dt)
    call preMatrix(U,U_Rot,dt)
    
end subroutine

subroutine allocateMemory_pre
    !*******************************************************************
    !   Description: 
    !	Date: 2016-4-28
    !   Modification: 
    !*******************************************************************
    implicit none
    allocate( Palf_c(nedges) )
    allocate( Palf_v(nedges) )
    allocate( Plamda_c(nedges) )
    allocate( Plamda_v(nedges) )
    allocate( SPalf_c(nedges) )
    allocate( SPalf_v(nedges) )
    allocate( SPlamda_c(nedges) )
    allocate( SPlamda_v(nedges) )
    allocate( PinvM(5,5,ncells) )
    allocate( MinvSP(5,5,ncells) )
    
end subroutine


subroutine deallocateMemory_pre
    !*******************************************************************
    !   Description: 
    !	Date: 2016-4-28
    !   Modification: 
    !*******************************************************************
    implicit none
    deallocate( Palf_c )
    deallocate( Palf_v )
    deallocate( Plamda_c )
    deallocate( Plamda_v )
    deallocate( SPalf_c )
    deallocate( SPalf_v )
    deallocate( SPlamda_c )
    deallocate( SPlamda_v )
    deallocate( PinvM )
    deallocate( MinvSP )
    
end subroutine

subroutine  spectral_P(U_av,U_Rot,muL_av,muT_av ,dt)
    !*******************************************************************
    !   Description: 
    !	Date: 2016-4-28
    !   Modification: 
    !*******************************************************************
    
    implicit none
    real(8),intent(in)::U_av(:,:),U_Rot(:,:),muL_av(:),muT_av(:)
    real(8),intent(out)::dt(:)
    integer::i
    integer::ncl,ncr
    real(8)::ut,vt,ux,uy,Vn                    
    real(8)::c,M2,Min2
    real(8)::beta
    
    !the first cycle to calculate the d2W and the spectral radius
    Plamda_c = 0.0d0
    Plamda_v = 0.0d0
    do i = 1,nedges

        ncl=iedge(3,i)
        ncr=iedge(4,i)
        
        ut =  0.5d0 * ( U_Rot(1,iedge(1,i)) + U_Rot(1,iedge(2,i)) ) 
        vt =  0.5d0 * ( U_Rot(2,iedge(1,i)) + U_Rot(2,iedge(2,i)) )
       
        ux =  U_av(2,i) - ut
        uy =  U_av(3,i) - vt
        Vn =  ux* vector(1,i)/ds(i) + uy * vector(2,i)/ds(i)

        c=sqrt( gamma*R*U_av(6,i) )
        M2 = (ux**2+uy**2)/c**2
        Min2 = kpre*Ma_inf**2
        
        beta = max( min(M2,1.0d0),Min2)
        
        ! the spectral radius used to calculate the artificial dissipation in the JST scheme
     
        Palf_c(i) = ( 0.5d0*( beta+1.0d0)*abs(Vn) + 0.5d0* sqrt( (beta-1.0d0)**2*Vn**2 + 4.0d0*beta*c**2) )*ds(i)
        Palf_v(i)= max( 4.0d0/3.0d0/U_av(1,i),gamma/U_av(1,i) ) *(1.0d0+beta*(gamma-1.0d0))*( muL_av(i)/PrL+muT_av(i)/PrT )
        
        Plamda_c(ncl) = Plamda_c(ncl) + Palf_c(i)
        Plamda_v(ncl) = Plamda_v(ncl) + Palf_v(i)*ds(i)**2/vol(ncl)   
        if ( ncr > 0) then
            Plamda_c(ncr) = Plamda_c(ncr) + Palf_c(i) 
            Plamda_v(ncr) = Plamda_v(ncr) + Palf_v(i)*ds(i)**2/vol(ncr)   
        end if   
    end do
    
    dt = CFL*vol/Plamda_c
    write(*,*)  maxval(dt) 
    
end subroutine

  
subroutine spectral_SP(U_av,U_Rot,muL_av,muT_av ,dt)   
    !*******************************************************************
    !   Description:
    !	Date: 2016-4-28
    !   Modification: 
    !*******************************************************************
    implicit none
    real(8),intent(in)::U_av(:,:),U_Rot(:,:),muL_av(:),muT_av(:),dt(:)
    integer::i,j,k
    integer::ncl,ncr
    real(8)::ut,vt,ux,uy,Vn                    
    real(8)::c,M2,Min2
    real(8)::alpha,beta,b
    
    !allocate(test(5,5,ncells))
    
    SPlamda_c = 0.0d0
    SPlamda_v = 0.0d0
    !the second cycle 
    do i = 1,nedges
        ncl = iedge(3,i)
        ncr = iedge(4,i)
        
        ut =  0.5d0 * ( U_Rot(1,iedge(1,i)) + U_Rot(1,iedge(2,i)) ) 
        vt =  0.5d0 * ( U_Rot(2,iedge(1,i)) + U_Rot(2,iedge(2,i)) )
       
        ux =  U_av(2,i) - ut
        uy =  U_av(3,i) - vt
      
        Vn =  ux* vector(1,i)/ds(i) + uy * vector(2,i)/ds(i) 
       
        !--------------------
        c=sqrt( gamma*R*U_av(6,i) )
        
        M2 = (ux**2+uy**2)/c**2
        Min2 = kpre*Ma_inf**2
        
        beta = min( max( M2,Min2),1.0d0)
        
        if( ncr < 0) then
            b= 1.0d0/(1.0d0 + 1.5d0*dt(ncl)/dt_r)   
        else
            b= 1.0d0/(1.0d0 + 1.5d0*0.5d0*( dt(ncl)+dt(ncr) )/dt_r)     
        end if
        
        alpha = beta/(b + (1.0d0-b)*beta)
          
        SPalf_c(i)= 0.5d0*b*( (alpha+1.0d0) * abs(Vn) + sqrt( (alpha-1.0d0)**2*Vn**2 + 4.0d0*alpha*c**2)   ) * ds(i)
        SPalf_v(i)= max( 4.0d0/3.0d0/U_av(1,i),gamma/U_av(1,i) ) *(1.0d0+beta*(gamma-1.0d0))*( muL_av(i)/PrL+muT_av(i)/PrT )
        
        SPlamda_c(ncl) = SPlamda_c(ncl) + SPalf_c(i)
        SPlamda_v(ncl) = SPlamda_v(ncl) + SPalf_v(i)*ds(i)**2/vol(ncl)   
        
        if( ncr > 0 )  then    !ncr less or equal to zero,the dissipation of this edge is set to zero
            SPlamda_c(ncr) = SPlamda_c(ncr) + SPalf_c(i)
            SPlamda_v(ncr) = SPlamda_v(ncr) + SPalf_v(i)*ds(i)**2/vol(ncr)    
        end if 
    end do
      
    
end subroutine


subroutine preMatrix(U,U_Rot,dt)
    !*******************************************************************
    !   Description: 
    !	Date: 2016-4-28
    !   Modification: 
    !*******************************************************************
    implicit none
    real(8),intent(in)::U(:,:),U_Rot(:,:),dt(:)
    integer::i,j,k
    integer::ncl,ncr

    real(8)::preM(5,5),preP(5,5),preSP(5,5)
    real(8)::preInvM(5,5),preInvP(5,5),preInvSP(5,5)
    real(8)::preArt(5,5)   

    real(8),allocatable::test(:,:,:)
    
    real(8)::Cv,Cp
    real(8)::c,T,H 
    real(8)::var1,var2
    real(8)::q2,theta
    real(8)::ut,vt,ux,uy,Vn                    
    real(8)::M2,Min2
    real(8)::alpha,beta,b
    
    allocate(test(5,5,ncells))
    
    Cv = R/(gamma-1.0d0)
    Cp = gamma*R/(gamma-1.0d0)
   
    do i = 1,ncells
        !------------------------------------------------------------------------------
        ! left cell
        if( icell(3,i)/=icell(4,i) ) then
            ut = 0.25d0 * sum( U_Rot(1,icell(:,i)) )
            vt = 0.25d0 * sum( U_Rot(2,icell(:,i)) )
        else
            ut = 1.0d0/3.0d0 * sum( U_Rot(1,icell(1:3,i)) )
            vt = 1.0d0/3.0d0 * sum( U_Rot(2,icell(1:3,i)) )
        end if
        ux =  U(2,i) - ut
        uy =  U(3,i) - vt
        !--------------------
        T = U(5,i)/U(1,i)/R
        c=sqrt( gamma*R*T)
        q2 = U(2,i)**2 + U(3,i)**2
         
        M2 = (ux**2+uy**2)/c**2
        Min2 = kpre*Ma_inf**2
        beta = max( min(M2,1.0d0),Min2)
        
        theta = ( 1.0d0+(gamma-1.0d0)*beta )/(beta*c**2)
         
        H = Cv*T + 0.5d0*q2 + U(5,i)/U(1,i)
        !----------------------------------------------
        ! M = dW/dU
        preM = 0.0d0
        
        var1 = U(1,i)/U(5,i)
        var2= Cp*U(1,i)*var1 - U(1,i)/T
        
        preM(1,1) =  var1
        preM(1,5) = -U(1,i)/T
        !row 2
        preM(2,1) =  var1*U(2,i)
        preM(2,2) =  U(1,i)
        preM(2,5) = -U(1,i)/T*U(2,i)
        !row 3
        preM(3,1) =  var1*U(3,i)
        preM(3,3) =  U(1,i)
        preM(3,5) = -U(1,i)/T*U(3,i)
        !row 4
        preM(4,4) =  U(1,i)
        !row 5
        preM(5,1) =  var1*H-1.0d0
        preM(5,2:3) = U(1,i)*U(2:3,i)
        preM(5,5) = -U(1,i)/T*H + Cp*U(1,i)
        
        !---------
        ! the inverse of M
        preInvM = 0.0d0
        preInvM(1,1) = ( Cp*U(1,i)-U(1,i)/T*(H-q2)  )/var2
        preInvM(1,2:3)= -U(1,i)/T*U(2:3,i)/var2
        preInvM(1,5)  =  U(1,i)/T/var2
        ! row 2
        preInvM(2,1) = -U(2,i)/U(1,i)
        preInvM(2,2) = 1.0d0/U(1,i)
        ! row 3
        preInvM(3,1) = -U(3,i)/U(1,i)
        preInvM(3,3) = 1.0d0/U(1,i)
        ! row 4
        preInvM(4,4) = 1.0d0/U(1,i)
        ! row 5
        preInvM(5,1) = (1.0d0 - var1*(H-q2) )/var2
        
        preInvM(5,2:3) = -var1*U(2:3,i)/var2
        preInvM(5,5) = var1/var2
        
        !test(:,:,i) = matmul(preM,preInvM)
        !------------------------------------------------
        ! precondtion matrix Weiss P
        preP = preM
        
        var1 = theta
        var2 = Cp*U(1,i)*var1 - U(1,i)/T
        
        ! vol 1
        preP(1,1) = var1
        preP(2:3,1) = var1*U(2:3,i)
        preP(5,1) = var1*H -1.0d0
        
        ! the inverse of P 
        ! row 1 
        preInvP = preInvM
        
        preInvP(1,1) = ( Cp*U(1,i)-U(1,i)/T*(H-q2)  )/var2
        preInvP(1,2:3)= -U(1,i)/T*U(2:3,i)/var2
        preInvP(1,5)  =  U(1,i)/T/var2
        ! row 5
        preInvP(5,1) = (1.0d0 - var1*(H-q2) )/var2
        preInvP(5,2:3) = -var1*U(2:3,i)/var2
        preInvP(5,5) = var1/var2
       
        !test(:,:,i) = matmul(preP,preInvP)
        ! -------------------------------------------------
        ! the SP
        preSP = preM

        b = 1.0d0/(1.0d0 + 1.5d0*dt(i)/dt_r)

        var1 = b*theta -(b-1.0d0)*U(1,i)/U(5,i)
        var2= Cp*U(1,i)*var1 - U(1,i)/T
         
        ! vol 1
        preSP(1,1) = var1
        preSP(2:3,1) = var1*U(2:3,i)
        preSP(5,1) = var1*H -1.0d0
        
        preSP = 1.0d0/b*preSP
        
        ! the inverse of SP 
        ! row 1 
        preInvSP = preInvM
        
        preInvSP(1,1) = ( Cp*U(1,i)-U(1,i)/T*(H-q2)  )/var2
        preInvSP(1,2:3)= -U(1,i)/T*U(2:3,i)/var2
        preInvSP(1,5)  =  U(1,i)/T/var2
        ! row 5
        preInvSP(5,1) = (1.0d0 - var1*(H-q2) )/var2
        preInvSP(5,2:3) = -var1*U(2:3,i)/var2
        preInvSP(5,5) = var1/var2
       
        preInvSP = b*preInvSP
        
         !test(:,:,i) = matmul(preSP,preInvSP)
        !------------------------------------------------------
        PinvM(:,:,i) = matmul( preP,preInvM )
         
        !SPinvM(:,:,i) = matmul(preSP, preInvM)
        MinvSP(:,:,i) = matmul( preM, preInvSP)
        
    end do

end subroutine

end module
