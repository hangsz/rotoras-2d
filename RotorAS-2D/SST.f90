    !*******************************************************************
    !	Copyright(c), 2016-2026, Sun Zhenhang
    !   All rights reserved.
    !	
    !	File name: 
    !	Version: 2.0
    !	Author:
    !	Date: 2016-4-28
    !
    !   Description: SST model 
    !	
    !*******************************************************************
    
module SST
    use controlDict
    use gridInfo
    
    implicit none

    real(8),allocatable,save:: U_SST(:,:) , U_av_SST(:,:)
    
    real(8),allocatable,save:: W_SST(:,:)
                        
    real(8),allocatable,save:: Q_SST(:,:),dQdW_SST(:,:)
    
    real(8),allocatable,save:: Grad_SST(:,:,:),Grad_W_SST(:,:,:),Fc_SST(:,:),Fv_SST(:,:),Dissi_SST(:,:),Rsi_SST(:,:)
    
    
    real(8),parameter::C1_SST = 5.0d0,C2_SST = 1.0d0, L_SST = 20.0d0 
    real(8),parameter::a1_SST = 0.31d0,beta_star_SST = 0.09d0 , kapa_SST = 0.41d0
    
    real(8),parameter::sigma_K1_SST = 0.85d0 , sigma_omg1_SST = 0.5d0 , beta1_SST=0.075d0  , Cw1_SST = 0.533d0
    real(8),parameter::sigma_K2_SST = 1.0d0  , sigma_omg2_SST = 0.856d0 , beta2_SST=0.0828d0 , Cw2_SST = 0.440d0  
    
    real(8),allocatable,save::sigma_K_SST(:), sigma_omg_SST(:), beta_SST(:), Cw_SST(:) 
    real(8),allocatable,save::f2_SST(:)
     
    ! dual time
    real(8),allocatable,save:: W_SST_n(:,:),W_SST_n1(:,:),QW_SST(:,:)
contains    
         
   !  turbulent  model--kw-SST
   include "allocateMemory_SST.f90"
   include "meanEdge_SST.f90"
   include "conFlux_SST.f90"
   include "artDissipation_SST.f90"
   include "gradient_SST.f90"
   include "visFlux_SST.f90"
   include "Resi_SST.f90"
   include "getMut_SST.f90"
   
   !include "LU_SGS_SST.f90"
end module


    