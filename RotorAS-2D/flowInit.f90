subroutine flowInit
    !*******************************************************************
    !   Description: get all the flow properties from input
    !	Date: 2016-4-28
    !   Modification: 
    !*******************************************************************
    implicit none
    
    real(8)::V_temp
        
    a_inf=sqrt(gamma*p_inf/rou_inf)
        
    V_temp = Ma_inf*a_inf 
    u_inf= V_temp*cosd(att)        
    v_inf= V_temp*sind(att)

    muL_inf = rou_inf*V_temp/Re_inf 
        
    !rou_inf =1.0d0
    !p_inf = 1.0d0/gamma/ma_inf**2
    !!V_temp = 1.0d0
    !u_inf=cosd(att)        
    !v_inf=sind(att)
    !muL_inf =1.0d0/Re_inf 
    !a_inf=1.0d0/Ma_inf
    !
    
    if(isReverse == 1)  then   
        u_inf = -u_inf
        v_inf = -v_inf
    end if
    
    
    U_Rot = 0.0d0
     
    if( moveNum== 0 )  then
        call flowInit_static  
    else
        call flowInit_dynamic
    end if
    
end subroutine
    